﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Reflection;
using System.Threading;

namespace XDFileDialog.Help
{
    internal static class FormHepler
    {
        /// <summary>
        /// UI线程的同步上下文
        /// </summary>
        public static SynchronizationContext m_SyncContext { set; get; }
        /// <summary>
        /// 实体类给控件赋值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        /// <param name="col"></param>
        public static void SetFieldToControl<T>(T t, params Control[] col)
        {
            List<PropertyInfo> list = t.GetType().GetProperties().ToList<PropertyInfo>();

            foreach (var item in list)
            {
                object value = item.GetValue(t, null);
                if (value != null)
                {
                    try
                    {
                        if (value.GetType().ToString()!="System.DateTime")
                        {
                            col.First(c => c.Name.ToLower() == ("txt" + item.Name).ToString().ToLower()).Text = value.ToString();
                        }
                        else
                        {
                            col.First(c => c.Name.ToLower() == ("txt" + item.Name).ToString().ToLower()).Text = Convert.ToDateTime(value).ToString("yyyy-MM-dd HH:mm:ss");
                        }
                        
                    }
                    catch
                    {
                    }

                }
            }
        }
    }
}
