﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Management;

namespace XDFileDialog.Help
{
    public static class DiskHelp
    {
        /// <summary>
        /// 盘符集合
        /// </summary>
        public static List<DiskInfo> DiskInfo { set; get; }
        static DiskHelp()
        {
            try
            {
                SelectQuery selectQuery = new SelectQuery("select * from win32_logicaldisk");
                ManagementObjectSearcher searcher = new ManagementObjectSearcher(selectQuery);
                DiskInfo = new List<Help.DiskInfo>();
                foreach (ManagementObject disk in searcher.Get())
                {
                    DiskInfo info = new Help.DiskInfo();
                    //盘符
                    info.Name = disk["Name"].ToString();
                    //卷标
                    try
                    {
                        info.VolumeName = disk["VolumeName"].ToString();
                    }
                    catch
                    {
                        //listView1.Items[i].SubItems.Add("设备未准备好");
                    }
                    //驱动器类型
                    string driveType;
                    try
                    {
                        driveType = disk["DriveType"].ToString();
                        switch (driveType)
                        {
                            case "0":
                                info.DriveType = DriveType.未知设备;
                                break;
                            case "1":
                                info.DriveType = DriveType.未分区;
                                break;
                            case "2":
                                info.DriveType = DriveType.可移动磁盘;
                                break;
                            case "3":
                                info.DriveType = DriveType.硬盘;
                                break;
                            case "4":
                                info.DriveType = DriveType.网络驱动器;
                                break;
                            case "5":
                                info.DriveType = DriveType.光驱;
                                break;
                            case "6":
                                info.DriveType = DriveType.内存磁盘;
                                break;
                        }
                    }
                    catch
                    {
                        info.DriveType = DriveType.未知设备;
                    }
                    //容量
                    try
                    {
                        info.Size = GetSizeUseUnit(disk["Size"].ToString());
                    }
                    catch
                    {
                        info.Size = "设备未准备好";
                    }
                    //剩余空间
                    try
                    {
                        info.FreeSpace = GetSizeUseUnit(disk["FreeSpace"].ToString());
                    }
                    catch
                    {
                        info.FreeSpace = "设备未准备好";
                    }
                    DiskInfo.Add(info);
                }
            }
            catch
            {
            }
        }
        /// <summary>
        /// 获取硬盘集合
        /// </summary>
        /// <returns></returns>
        public static List<DiskInfo> GetHardDisk()
        {
            return DiskInfo.Where(c => c.DriveType == DriveType.硬盘).ToList();
        }
        private static string GetSizeUseUnit(string size)
        {
            double dSpace = Convert.ToDouble(size);
            string sSpace = dSpace.ToString("N");
            string[] tmp;
            string rtnSize = "0";
            tmp = sSpace.Split(',');
            switch (tmp.GetUpperBound(0))
            {
                case 0:
                    rtnSize = tmp[0] + " 字节";
                    break;
                case 1:
                    rtnSize = tmp[0] + "." + tmp[1].Substring(0, 2) + " K";
                    break;
                case 2:
                    rtnSize = tmp[0] + "." + tmp[1].Substring(0, 2) + " M";
                    break;
                case 3:
                    rtnSize = tmp[0] + "." + tmp[1].Substring(0, 2) + " G";
                    break;
                case 4:
                    rtnSize = tmp[0] + "." + tmp[1].Substring(0, 2) + " T";
                    break;
            }
            return rtnSize;
        }
        public enum DriveType
        {
            未知设备 = 0,
            未分区 = 1,
            可移动磁盘 = 2,
            硬盘 = 3,
            网络驱动器 = 4,
            光驱 = 5,
            内存磁盘 = 6,
        }
    }
    public class DiskInfo
    {
        /// <summary>
        /// 盘符
        /// </summary>
        public string Name { set; get; }
        /// <summary>
        /// 卷标
        /// </summary>
        public string VolumeName { set; get; }
        /// <summary>
        /// 驱动器类型
        /// </summary>
        public XDFileDialog.Help.DiskHelp.DriveType DriveType { set; get; }
        /// <summary>
        /// 容量
        /// </summary>
        public string Size { set; get; }
        /// <summary>
        /// 剩余空间
        /// </summary>
        public string FreeSpace { set; get; }
    }
}
