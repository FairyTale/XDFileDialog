﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace XDFileDialog.Help
{
    internal class PictureHelp
    {
        /// <summary>
        /// Image转Icon
        /// </summary>
        /// <param name="img"></param>
        /// <returns></returns>
        public static Icon ImageToIcon(Image img)
        {
            return Icon.FromHandle(((Bitmap)img).GetHicon());
        }
        /// <summary>
        /// Icon转Image
        /// </summary>
        /// <param name="img"></param>
        /// <returns></returns>
        public static Image IconToImage(Icon icon)
        {
            return Image.FromHbitmap(icon.ToBitmap().GetHbitmap());
        }
        /// <summary>
        /// Icon转Bitmap
        /// </summary>
        /// <param name="icon"></param>
        /// <returns></returns>
        public static Bitmap IconToBitmap(Icon icon)
        {
            return icon.ToBitmap();
        }
        /// <summary>
        /// 图片格式转换
        /// </summary>
        /// <param name="sourcePath"></param>
        /// <param name="distationPath"></param>
        /// <param name="format"></param>
        public void ImageFormatter(string sourcePath, string distationPath, string format)
        {
            System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(sourcePath);
            switch (format.ToLower())
            {
                case "bmp":
                    bitmap.Save(distationPath, System.Drawing.Imaging.ImageFormat.Bmp);
                    break;
                case "emf":
                    bitmap.Save(distationPath, System.Drawing.Imaging.ImageFormat.Emf);
                    break;
                case "gif":
                    bitmap.Save(distationPath, System.Drawing.Imaging.ImageFormat.Gif);
                    break;
                case "ico":
                    bitmap.Save(distationPath, System.Drawing.Imaging.ImageFormat.Icon);
                    break;
                case "jpg":
                    bitmap.Save(distationPath, System.Drawing.Imaging.ImageFormat.Jpeg);
                    break;
                case "png":
                    bitmap.Save(distationPath, System.Drawing.Imaging.ImageFormat.Png);
                    break;
                case "tif":
                    bitmap.Save(distationPath, System.Drawing.Imaging.ImageFormat.Tiff);
                    break;
                case "wmf":
                    bitmap.Save(distationPath, System.Drawing.Imaging.ImageFormat.Wmf);
                    break;
                default: throw new Exception("无法转换此格式！");
            }
        }
    }
}
