﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterFace
{
    internal class EditHelp<T>
    {
        //服务器路径
        public string path = System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase;
        public EditHelp(string paths)
        {
            this.path = this.path + "\\" + paths;
        }
        /// <summary>
        /// 添加服务器
        /// </summary>
        /// <param name="model"></param>
        public void Add<T>(T t)
        {
            List<T> lUploadConfig = new List<T>();
            try
            {
                lUploadConfig = XML.Deserialization<List<T>>(path);
                if (lUploadConfig != null)
                {

                    Type type = t.GetType();
                    object value = type.GetProperty("Id").GetValue(t, null);
                    int a = lUploadConfig.Max(c => Convert.ToInt32(c.GetType().GetProperty("Id").GetValue(c, null)));
                    type.GetProperty("Id").SetValue(t, a + 1, null);
                }
                else
                {
                    lUploadConfig = new List<T>();
                    Type type = t.GetType();
                    object value = type.GetProperty("Id").GetValue(t, null);
                    type.GetProperty("Id").SetValue(t, 1, null);
                }
            }
            catch
            {
                Type type = t.GetType();
                object value = type.GetProperty("Id").GetValue(t, null);
                if (value is int)
                {
                    type.GetProperty("Id").SetValue(t, 1, null);
                }

            }
            finally
            {
                lUploadConfig.Add(t);
            }
            XML.Serialization<List<T>>(lUploadConfig, path);
        }
        /// <summary>
        /// 删除服务器
        /// </summary>
        /// <param name="model"></param>
        public void Del(string Id)
        {
            List<T> lUploadConfig = XML.Deserialization<List<T>>(path);
            lUploadConfig = (from c in lUploadConfig
                             where c.GetType().GetProperty("Id").GetValue(c, null).ToString() != Id
                             select c).ToList();
            XML.Serialization<List<T>>(lUploadConfig, path);
        }
        /// <summary>
        /// 更新服务器
        /// </summary>
        /// <param name="model"></param>
        public void Update(T t)
        {
            List<T> lUploadConfig = XML.Deserialization<List<T>>(path);
            lUploadConfig = (from c in lUploadConfig
                             where c.GetType().GetProperty("Id").GetValue(c, null).ToString() !=
                             t.GetType().GetProperty("Id").GetValue(t, null).ToString()
                             select c).ToList();
            lUploadConfig.Add(t);
            XML.Serialization<List<T>>(lUploadConfig, path);
        }
        //获取列表
        public List<T> List()
        {
            List<T> lUploadConfig = XML.Deserialization<List<T>>(path);
            if (lUploadConfig==null)
            {
                lUploadConfig = new List<T>();
            }
            return lUploadConfig;
        }
        public T query(string Id)
        {
            List<T> lUploadConfig = XML.Deserialization<List<T>>(path);
            lUploadConfig = (from c in lUploadConfig
                             where c.GetType().GetProperty("Id").GetValue(c, null).ToString() == Id
                             select c).ToList();
            if (lUploadConfig.Count > 0)
            {
                return lUploadConfig[0];
            }
            else
            {
                return default(T);
            }

        }
    }
}
