﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;

namespace XDFileDialog
{
    /// <summary>
    /// 特殊系统路径
    /// </summary>
    internal class SpecialSysPathHelp
    {
        /// <summary>
        /// 从注册表中特殊系统路径
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string GetFolderPath(PathType type)
        {
            RegistryKey folders;
            folders = OpenRegistryPath(Registry.CurrentUser, @"\software\microsoft\windows\currentversion\explorer\shell folders");
            return folders.GetValue(type.ToString()).ToString();
        }
        /// <summary>
        /// 特殊系统路径
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string GetFolderPath(Environment.SpecialFolder type)
        {
            return Environment.GetFolderPath(type);
        }
        public enum PathType
        {
            // Windows用户桌面路径
            Desktop,
            // Windows用户字体目录路径
            Fonts,
            // Windows用户网络邻居路径
            Nethood,
            // Windows用户我的文档路径
            Personal,
            // Windows用户开始菜单程序路径
            Programs,
            // Windows用户存放用户最近访问文档快捷方式的目录路径
            Recent,
            // Windows用户发送到目录路径
            Sendto,
            // Windows用户开始菜单目录路径
            Startmenu,
            // Windows用户开始菜单启动项目录路径
            Startup,
            // Windows用户收藏夹目录路径
            Favorites,
            // Windows用户网页历史目录路径
            History,
            // Windows用户Cookies目录路径
            Cookies,
            // Windows用户Cache目录路径
            Cache,
            // Windows用户应用程式数据目录路径
            Appdata,
            // Windows用户打印目录路径
            Printhood
        }
        private static RegistryKey OpenRegistryPath(RegistryKey root, string s)
        {
            s = s.Remove(0, 1) + @"\";
            while (s.IndexOf(@"\") != -1)
            {
                root = root.OpenSubKey(s.Substring(0, s.IndexOf(@"\")));
                s = s.Remove(0, s.IndexOf(@"\") + 1);
            }
            return root;
        }
    }
}
