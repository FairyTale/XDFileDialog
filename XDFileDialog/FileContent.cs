﻿using DSkin.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DSkin.DirectUI;
using DSkin.Forms;
using XDFileDialog.Template;
using XDFileDialog.Help;

namespace XDFileDialog
{
    //protected
    internal partial class FileContent : DSkin.Controls.DSkinUserControl
    {
        /// <summary>
        /// 路径
        /// </summary>
        public string Path { set; get; }
        /// <summary>
        /// 整个路径
        /// </summary>
        public string PathFileName { set; get; }
        /// <summary>
        /// 文件名和扩展名
        /// </summary>
        public string SafeFileName { set; get; }

        public FileContent()
        {
            InitializeComponent();
        }
        /// <summary>
        /// 我的电脑
        /// </summary>
        public void GetMyComputer()
        {
            txtPath.Text = "计算机";
            dSkinGridList1.Visible = false;
            dSkinListBox1.Visible = true;
            dSkinListBox1.Items.Clear();
            dSkinListBox1.Ulmul = true;
            dSkinListBox1.ItemSize = new Size(270, 70);
            dSkinListBox1.SelectionMode = SelectionModes.Radio;
            dSkinListBox1.Template = typeof(MyComputerTemplate);
            dSkinListBox1.DataSource = DiskHelp.GetHardDisk();
        }
        /// <summary>
        /// 目录
        /// </summary>
        /// <param name="path"></param>
        public void GetCatalog(string path)
        {
            lock (this)
            {
                try
                {
                    dSkinGridList1.Visible = true;
                    dSkinListBox1.Visible = false;
                    txtPath.Text = path;
                    List<string> list = (List<string>)cboFileType.Tag;
                    var catalog = CatalogHelp.GetCatalog(path, list[cboFileType.SelectedIndex]);
                    catalog.Insert(0, new Catalog() { Name = "上一层" });
                    dSkinGridList1.DataSource = catalog;
                }
                catch (Exception ex)
                {
                }
            }
        }
        private void ReturnFile(string path, string FullName)
        {
            SafeFileName = FullName;
            Path = path;
            PathFileName = string.Concat(Path, "\\", SafeFileName);
            FileDialogFrm frm = (this.FindForm() as FileDialogFrm);
            frm.ReturnFile(PathFileName);
            frm.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.FindForm().Close();
        }

        private void dSkinGridList1_ItemClick(object sender, DSkinGridListMouseEventArgs e)
        {
            if (e.Item.Cells[1].Value.ToString() == "上一层")
            {
                System.IO.DirectoryInfo info = System.IO.Directory.GetParent(txtPath.Text);
                if (info == null)
                {
                    GetMyComputer();
                }
                else
                {
                    GetCatalog(info.FullName);
                }

            }
            else
            {
                if (e.Item.Cells[3].Value.ToString() != "文件夹")
                {
                    txtFileName.Text = e.Item.Cells[1].Value.ToString();
                }
            }
        }

        private void dSkinListBox1_AcceptTask(object sender, AcceptTaskEventArgs e)
        {
            if (e.TaskName == "计算机")
            {
                GetCatalog(e.Data.ToString() + "\\");
            }
        }

        private void dSkinGridList1_ItemDoubleClick(object sender, DSkinGridListMouseEventArgs e)
        {
            if (e.Item.Cells[1].Value.ToString() != "上一层")
            {
                if (e.Item.Cells[3].Value.ToString() != "文件夹")
                {
                    ReturnFile(txtPath.Text, e.Item.Cells[1].Value.ToString());
                }
                else
                    GetCatalog(txtPath.Text = string.Concat(txtPath.Text, "\\", e.Item.Cells[1].Value.ToString()));

            }
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtFileName.Text.Trim()))
            {
                MessageBox.Show("请先选择一个文件");
            }
            else
            {
                ReturnFile(txtPath.Text, txtFileName.Text.Trim());
            }
        }

        private void cboFileType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (txtPath.Text != "计算机")
            {
                GetCatalog(txtPath.Text);
            }

        }

    }
}
