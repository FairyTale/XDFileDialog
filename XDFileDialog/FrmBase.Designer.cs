﻿namespace XDFileDialog
{
    partial class FrmBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmBase));
            this.SuspendLayout();
            // 
            // FrmBase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.CornflowerBlue;
            this.BorderColor = System.Drawing.Color.MediumBlue;
            this.CaptionColor = System.Drawing.Color.White;
            this.CaptionFont = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.CaptionOffset = new System.Drawing.Point(0, 3);
            this.CaptionShowMode = DSkin.TextShowModes.Ordinary;
            this.ClientSize = new System.Drawing.Size(905, 500);
            this.CloseBox.BorderHoverColor = System.Drawing.Color.GreenYellow;
            this.CloseBox.BorderNormalColor = System.Drawing.Color.Transparent;
            this.CloseBox.BorderPressColor = System.Drawing.Color.GreenYellow;
            this.CloseBox.HoverBackColor = System.Drawing.Color.GreenYellow;
            this.CloseBox.NormalColor = System.Drawing.Color.White;
            this.CloseBox.PressBackColor = System.Drawing.Color.GreenYellow;
            this.DrawIcon = false;
            this.HaloColor = System.Drawing.Color.Transparent;
            this.HaloSize = 0;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaxBox.BorderHoverColor = System.Drawing.Color.GreenYellow;
            this.MaxBox.BorderPressColor = System.Drawing.Color.GreenYellow;
            this.MaxBox.HoverBackColor = System.Drawing.Color.GreenYellow;
            this.MaxBox.PressBackColor = System.Drawing.Color.GreenYellow;
            this.MinBox.BorderHoverColor = System.Drawing.Color.GreenYellow;
            this.MinBox.BorderPressColor = System.Drawing.Color.GreenYellow;
            this.MinBox.HoverBackColor = System.Drawing.Color.GreenYellow;
            this.MinBox.NormalColor = System.Drawing.Color.White;
            this.MinBox.PressBackColor = System.Drawing.Color.GreenYellow;
            this.Name = "FrmBase";
            this.NormalBox.BorderHoverColor = System.Drawing.Color.GreenYellow;
            this.NormalBox.BorderPressColor = System.Drawing.Color.GreenYellow;
            this.NormalBox.HoverBackColor = System.Drawing.Color.GreenYellow;
            this.NormalBox.PressBackColor = System.Drawing.Color.GreenYellow;
            this.Text = "frmBase";
            this.ResumeLayout(false);

        }

        #endregion
    }
}