﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace XDFileDialog
{
    public partial class XDOpenFileDialog : Component
    {
        /// <summary>
        /// 路径
        /// </summary>
        public string Path
        {
            set
            {
                frm.Path = value;
            }
            get
            {
                return frm.Path;
            }
        }
        /// <summary>
        /// 整个路径
        /// </summary>
        public string PathFileName
        {
            set
            {
                frm.PathFileName = value;
            }
            get
            {
                return frm.PathFileName;
            }
        }
        /// <summary>
        /// 文件名和扩展名
        /// </summary>
        public string SafeFileName
        {
            set
            {
                frm.SafeFileName = value;
            }
            get
            {
                return frm.SafeFileName;
            }
        }
        /// <summary>
        /// 要在对话框中显示的文件筛选器，例如，"文本文件(*.txt)|*.txt|所有文件(*.*)|*.*"
        /// </summary>
        [Description("要在对话框中显示的文件筛选器，例如，文本文件(*.txt)|*.txt|所有文件(*.*)|*.*")]
        [Category("文件选择器")]
        public string Filter { set; get; }
        /// <summary>
        /// 在对话框中选择的文件筛选器的索引，如果选第一项就设为1
        /// </summary>
        [Description("在对话框中选择的文件筛选器的索引，如果选第一项就设为1")]
        [Category("文件选择器")]
        public int FilterIndex { set; get; }


        public delegate void FileOkEventArgs(object sender, string path);
        /// <summary>
        /// 获取到文件路径
        /// </summary>
        [Description("文件选择时触发的事件")]
        [Category("文件选择器")]
        public event FileOkEventArgs FileOk;
        FileDialogFrm frm = null;
        public XDOpenFileDialog()
        {
            InitializeComponent();
            FilterIndex = 0;
            Filter = "所有文件(*.*)|*.*";
            if (frm == null)
            {
                frm = new FileDialogFrm();
                frm.FileOk += new FileDialogFrm.FileOkEventArgs(frm_FileOk);
            }
        }
        public XDOpenFileDialog(IContainer container)
        {
            container.Add(this);
            InitializeComponent();
            FilterIndex = 0;
            Filter = "所有文件(*.*)|*.*";
            if (frm == null)
            {
                frm = new FileDialogFrm();
                frm.FileOk += new FileDialogFrm.FileOkEventArgs(frm_FileOk);
            }
        }
        void frm_FileOk(object sender, string path)
        {
            if (FileOk != null)
            {
                FileOk(this, path);
            }
        }

        public DialogResult ShowDialog()
        {
            frm.Reset();
            frm.getFileType(Filter,FilterIndex);
            return frm.ShowDialog();
        }
    }
}
