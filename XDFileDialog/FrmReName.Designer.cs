﻿namespace XDFileDialog
{
    partial class FrmReName
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmReName));
            this.dSkinPanel1 = new DSkin.Controls.DSkinPanel();
            this.dSkinLabel1 = new DSkin.Controls.DSkinLabel();
            this.txtName = new DSkin.Controls.DSkinTextBox();
            this.btnOK = new DSkin.Controls.DSkinButton();
            this.dSkinPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dSkinPanel1
            // 
            this.dSkinPanel1.BackColor = System.Drawing.Color.White;
            this.dSkinPanel1.Controls.Add(this.btnOK);
            this.dSkinPanel1.Controls.Add(this.txtName);
            this.dSkinPanel1.Controls.Add(this.dSkinLabel1);
            this.dSkinPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dSkinPanel1.Location = new System.Drawing.Point(4, 34);
            this.dSkinPanel1.Name = "dSkinPanel1";
            this.dSkinPanel1.RightBottom = ((System.Drawing.Image)(resources.GetObject("dSkinPanel1.RightBottom")));
            this.dSkinPanel1.Size = new System.Drawing.Size(445, 121);
            this.dSkinPanel1.TabIndex = 0;
            this.dSkinPanel1.Text = "dSkinPanel1";
            // 
            // dSkinLabel1
            // 
            this.dSkinLabel1.Location = new System.Drawing.Point(33, 24);
            this.dSkinLabel1.Name = "dSkinLabel1";
            this.dSkinLabel1.Size = new System.Drawing.Size(42, 16);
            this.dSkinLabel1.TabIndex = 0;
            this.dSkinLabel1.Text = "名称：";
            // 
            // txtName
            // 
            this.txtName.BitmapCache = false;
            this.txtName.Location = new System.Drawing.Point(81, 19);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(310, 21);
            this.txtName.TabIndex = 1;
            this.txtName.TransparencyKey = System.Drawing.Color.Empty;
            this.txtName.WaterFont = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtName.WaterText = "";
            this.txtName.WaterTextOffset = new System.Drawing.Point(0, 0);
            // 
            // btnOK
            // 
            this.btnOK.AdaptImage = true;
            this.btnOK.BaseColor = System.Drawing.Color.White;
            this.btnOK.ButtonBorderColor = System.Drawing.Color.Gray;
            this.btnOK.ButtonBorderWidth = 1;
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnOK.HoverColor = System.Drawing.Color.Empty;
            this.btnOK.HoverImage = null;
            this.btnOK.IsPureColor = true;
            this.btnOK.Location = new System.Drawing.Point(162, 67);
            this.btnOK.Name = "btnOK";
            this.btnOK.NormalImage = null;
            this.btnOK.PressColor = System.Drawing.Color.Empty;
            this.btnOK.PressedImage = null;
            this.btnOK.Radius = 10;
            this.btnOK.ShowButtonBorder = true;
            this.btnOK.Size = new System.Drawing.Size(100, 40);
            this.btnOK.TabIndex = 2;
            this.btnOK.Text = "确      定";
            this.btnOK.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnOK.TextPadding = 0;
            this.btnOK.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // FrmReName
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(453, 159);
            this.CloseBox.BorderHoverColor = System.Drawing.Color.GreenYellow;
            this.CloseBox.BorderNormalColor = System.Drawing.Color.Transparent;
            this.CloseBox.BorderPressColor = System.Drawing.Color.GreenYellow;
            this.CloseBox.HoverBackColor = System.Drawing.Color.GreenYellow;
            this.CloseBox.NormalColor = System.Drawing.Color.White;
            this.CloseBox.PressBackColor = System.Drawing.Color.GreenYellow;
            this.Controls.Add(this.dSkinPanel1);
            this.EnableAnimation = false;
            this.MaxBox.BorderHoverColor = System.Drawing.Color.GreenYellow;
            this.MaxBox.BorderPressColor = System.Drawing.Color.GreenYellow;
            this.MaxBox.HoverBackColor = System.Drawing.Color.GreenYellow;
            this.MaxBox.NormalColor = System.Drawing.Color.White;
            this.MaxBox.PressBackColor = System.Drawing.Color.GreenYellow;
            this.MinBox.BorderHoverColor = System.Drawing.Color.GreenYellow;
            this.MinBox.BorderPressColor = System.Drawing.Color.GreenYellow;
            this.MinBox.HoverBackColor = System.Drawing.Color.GreenYellow;
            this.MinBox.NormalColor = System.Drawing.Color.White;
            this.MinBox.PressBackColor = System.Drawing.Color.GreenYellow;
            this.Name = "FrmReName";
            this.NormalBox.BorderHoverColor = System.Drawing.Color.GreenYellow;
            this.NormalBox.BorderPressColor = System.Drawing.Color.GreenYellow;
            this.NormalBox.HoverBackColor = System.Drawing.Color.GreenYellow;
            this.NormalBox.NormalColor = System.Drawing.Color.White;
            this.NormalBox.PressBackColor = System.Drawing.Color.GreenYellow;
            this.Text = "重命名";
            this.dSkinPanel1.ResumeLayout(false);
            this.dSkinPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DSkin.Controls.DSkinPanel dSkinPanel1;
        private DSkin.Controls.DSkinTextBox txtName;
        private DSkin.Controls.DSkinLabel dSkinLabel1;
        private DSkin.Controls.DSkinButton btnOK;
    }
}