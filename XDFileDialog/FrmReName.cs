﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using InterFace;
using XDFileDialog.Model;

namespace XDFileDialog
{
    public partial class FrmReName : FrmBase
    {
        EditHelp<LeftModel> help = new EditHelp<LeftModel>(PathHelp.SysPach);
        public string LeftId { set; get; }
        public FrmReName()
        {
            InitializeComponent();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            LeftModel model = help.query(LeftId);
            model.Name = txtName.Text.Trim();
            help.Update(model);
            this.Close();
        }
    }
}
