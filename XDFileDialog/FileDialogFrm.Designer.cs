﻿namespace XDFileDialog
{
    partial class FileDialogFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FileDialogFrm));
            this.dSkinPanel1 = new DSkin.Controls.DSkinPanel();
            this.dSkinPanel4 = new DSkin.Controls.DSkinPanel();
            this.lstLeft = new DSkin.Controls.DSkinListBox();
            this.dSkinPanel2 = new DSkin.Controls.DSkinPanel();
            this.btnCustomLocation = new DSkin.Controls.DSkinButton();
            this.duiIcon1 = new DSkin.DirectUI.DuiIcon();
            this.dSkinPanel3 = new DSkin.Controls.DSkinPanel();
            this.dSkinLabel1 = new DSkin.Controls.DSkinLabel();
            this.fileContent1 = new XDFileDialog.FileContent();
            this.dSkinLabel2 = new DSkin.Controls.DSkinLabel();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.dSkinContextMenuStrip1 = new DSkin.Controls.DSkinContextMenuStrip();
            this.重命名ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.删除ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dSkinPanel1.SuspendLayout();
            this.dSkinPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lstLeft)).BeginInit();
            this.dSkinPanel2.SuspendLayout();
            this.dSkinPanel3.SuspendLayout();
            this.dSkinContextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dSkinPanel1
            // 
            this.dSkinPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dSkinPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(130)))), ((int)(((byte)(184)))));
            this.dSkinPanel1.Controls.Add(this.dSkinPanel4);
            this.dSkinPanel1.Controls.Add(this.dSkinPanel2);
            this.dSkinPanel1.Controls.Add(this.dSkinPanel3);
            this.dSkinPanel1.Location = new System.Drawing.Point(0, 0);
            this.dSkinPanel1.Name = "dSkinPanel1";
            this.dSkinPanel1.RightBottom = ((System.Drawing.Image)(resources.GetObject("dSkinPanel1.RightBottom")));
            this.dSkinPanel1.Size = new System.Drawing.Size(188, 714);
            this.dSkinPanel1.TabIndex = 0;
            this.dSkinPanel1.Text = "dSkinPanel1";
            // 
            // dSkinPanel4
            // 
            this.dSkinPanel4.BackColor = System.Drawing.Color.Transparent;
            this.dSkinPanel4.Controls.Add(this.lstLeft);
            this.dSkinPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dSkinPanel4.Location = new System.Drawing.Point(0, 54);
            this.dSkinPanel4.Name = "dSkinPanel4";
            this.dSkinPanel4.RightBottom = ((System.Drawing.Image)(resources.GetObject("dSkinPanel4.RightBottom")));
            this.dSkinPanel4.Size = new System.Drawing.Size(188, 603);
            this.dSkinPanel4.TabIndex = 1;
            this.dSkinPanel4.Text = "dSkinPanel4";
            // 
            // lstLeft
            // 
            this.lstLeft.BackColor = System.Drawing.Color.Transparent;
            this.lstLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstLeft.Location = new System.Drawing.Point(0, 0);
            this.lstLeft.Name = "lstLeft";
            this.lstLeft.ScrollBarWidth = 12;
            this.lstLeft.Size = new System.Drawing.Size(188, 603);
            this.lstLeft.TabIndex = 3;
            this.lstLeft.Text = "dSkinListBox1";
            this.lstLeft.Value = 0D;
            this.lstLeft.AcceptTask += new System.EventHandler<DSkin.DirectUI.AcceptTaskEventArgs>(this.lstLeft_AcceptTask);
            // 
            // dSkinPanel2
            // 
            this.dSkinPanel2.BackColor = System.Drawing.Color.Transparent;
            this.dSkinPanel2.Controls.Add(this.btnCustomLocation);
            this.dSkinPanel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dSkinPanel2.Location = new System.Drawing.Point(0, 657);
            this.dSkinPanel2.Name = "dSkinPanel2";
            this.dSkinPanel2.RightBottom = ((System.Drawing.Image)(resources.GetObject("dSkinPanel2.RightBottom")));
            this.dSkinPanel2.Size = new System.Drawing.Size(188, 57);
            this.dSkinPanel2.TabIndex = 0;
            this.dSkinPanel2.Text = "dSkinPanel2";
            // 
            // btnCustomLocation
            // 
            this.btnCustomLocation.AdaptImage = true;
            this.btnCustomLocation.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(73)))), ((int)(((byte)(146)))), ((int)(((byte)(206)))));
            this.btnCustomLocation.ButtonBorderColor = System.Drawing.Color.Transparent;
            this.btnCustomLocation.ButtonBorderWidth = 1;
            this.btnCustomLocation.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnCustomLocation.DUIControls.Add(this.duiIcon1);
            this.btnCustomLocation.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnCustomLocation.ForeColor = System.Drawing.Color.White;
            this.btnCustomLocation.HoverColor = System.Drawing.Color.Empty;
            this.btnCustomLocation.HoverImage = null;
            this.btnCustomLocation.IsPureColor = true;
            this.btnCustomLocation.Location = new System.Drawing.Point(19, 8);
            this.btnCustomLocation.Name = "btnCustomLocation";
            this.btnCustomLocation.NormalImage = null;
            this.btnCustomLocation.PressColor = System.Drawing.Color.Empty;
            this.btnCustomLocation.PressedImage = null;
            this.btnCustomLocation.Radius = 0;
            this.btnCustomLocation.ShowButtonBorder = true;
            this.btnCustomLocation.Size = new System.Drawing.Size(153, 40);
            this.btnCustomLocation.TabIndex = 1;
            this.btnCustomLocation.Text = "自定义位置";
            this.btnCustomLocation.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnCustomLocation.TextPadding = 0;
            this.btnCustomLocation.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.btnCustomLocation.Click += new System.EventHandler(this.btnCustomLocation_Click);
            // 
            // duiIcon1
            // 
            this.duiIcon1.DesignModeCanResize = false;
            this.duiIcon1.ForeColor = System.Drawing.Color.White;
            this.duiIcon1.Icon = DSkin.Common.FontAwesomeChars.icon_plus;
            this.duiIcon1.IconSize = 15F;
            this.duiIcon1.Location = new System.Drawing.Point(18, 12);
            this.duiIcon1.Name = "duiIcon1";
            this.duiIcon1.Size = new System.Drawing.Size(23, 23);
            // 
            // dSkinPanel3
            // 
            this.dSkinPanel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(96)))), ((int)(((byte)(144)))), ((int)(((byte)(198)))));
            this.dSkinPanel3.Controls.Add(this.dSkinLabel1);
            this.dSkinPanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.dSkinPanel3.Location = new System.Drawing.Point(0, 0);
            this.dSkinPanel3.Name = "dSkinPanel3";
            this.dSkinPanel3.RightBottom = ((System.Drawing.Image)(resources.GetObject("dSkinPanel3.RightBottom")));
            this.dSkinPanel3.Size = new System.Drawing.Size(188, 54);
            this.dSkinPanel3.TabIndex = 0;
            this.dSkinPanel3.Text = "dSkinPanel3";
            // 
            // dSkinLabel1
            // 
            this.dSkinLabel1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dSkinLabel1.ForeColor = System.Drawing.Color.White;
            this.dSkinLabel1.Location = new System.Drawing.Point(48, 15);
            this.dSkinLabel1.Name = "dSkinLabel1";
            this.dSkinLabel1.Size = new System.Drawing.Size(72, 24);
            this.dSkinLabel1.TabIndex = 1;
            this.dSkinLabel1.Text = "本地文档";
            // 
            // fileContent1
            // 
            this.fileContent1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.fileContent1.BackColor = System.Drawing.Color.Transparent;
            this.fileContent1.BitmapCache = false;
            this.fileContent1.Location = new System.Drawing.Point(191, 31);
            this.fileContent1.Margin = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.fileContent1.Name = "fileContent1";
            this.fileContent1.Path = null;
            this.fileContent1.PathFileName = null;
            this.fileContent1.RightBottom = ((System.Drawing.Image)(resources.GetObject("fileContent1.RightBottom")));
            this.fileContent1.SafeFileName = null;
            this.fileContent1.Size = new System.Drawing.Size(874, 683);
            this.fileContent1.TabIndex = 1;
            // 
            // dSkinLabel2
            // 
            this.dSkinLabel2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dSkinLabel2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dSkinLabel2.Location = new System.Drawing.Point(597, 10);
            this.dSkinLabel2.Name = "dSkinLabel2";
            this.dSkinLabel2.Size = new System.Drawing.Size(54, 18);
            this.dSkinLabel2.TabIndex = 2;
            this.dSkinLabel2.Text = "打开文件";
            // 
            // dSkinContextMenuStrip1
            // 
            this.dSkinContextMenuStrip1.Arrow = System.Drawing.Color.Black;
            this.dSkinContextMenuStrip1.Back = System.Drawing.SystemColors.Window;
            this.dSkinContextMenuStrip1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.dSkinContextMenuStrip1.BackRadius = 1;
            this.dSkinContextMenuStrip1.Base = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(200)))), ((int)(((byte)(254)))));
            this.dSkinContextMenuStrip1.DropDownImageSeparator = System.Drawing.Color.Silver;
            this.dSkinContextMenuStrip1.Fore = System.Drawing.Color.Black;
            this.dSkinContextMenuStrip1.HoverFore = System.Drawing.Color.White;
            this.dSkinContextMenuStrip1.ItemAnamorphosis = false;
            this.dSkinContextMenuStrip1.ItemBorder = System.Drawing.Color.Transparent;
            this.dSkinContextMenuStrip1.ItemBorderShow = false;
            this.dSkinContextMenuStrip1.ItemHover = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.dSkinContextMenuStrip1.ItemPressed = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.dSkinContextMenuStrip1.ItemRadius = 1;
            this.dSkinContextMenuStrip1.ItemRadiusStyle = DSkin.Common.RoundStyle.None;
            this.dSkinContextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.重命名ToolStripMenuItem,
            this.删除ToolStripMenuItem});
            this.dSkinContextMenuStrip1.ItemSplitter = System.Drawing.Color.White;
            this.dSkinContextMenuStrip1.Name = "dSkinContextMenuStrip1";
            this.dSkinContextMenuStrip1.RadiusStyle = DSkin.Common.RoundStyle.All;
            this.dSkinContextMenuStrip1.Size = new System.Drawing.Size(113, 48);
            this.dSkinContextMenuStrip1.SkinAllColor = true;
            this.dSkinContextMenuStrip1.TitleAnamorphosis = false;
            this.dSkinContextMenuStrip1.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(228)))), ((int)(((byte)(236)))));
            this.dSkinContextMenuStrip1.TitleRadius = 1;
            this.dSkinContextMenuStrip1.TitleRadiusStyle = DSkin.Common.RoundStyle.None;
            // 
            // 重命名ToolStripMenuItem
            // 
            this.重命名ToolStripMenuItem.Name = "重命名ToolStripMenuItem";
            this.重命名ToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.重命名ToolStripMenuItem.Text = "重命名";
            this.重命名ToolStripMenuItem.Click += new System.EventHandler(this.重命名ToolStripMenuItem_Click);
            // 
            // 删除ToolStripMenuItem
            // 
            this.删除ToolStripMenuItem.Name = "删除ToolStripMenuItem";
            this.删除ToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.删除ToolStripMenuItem.Text = "删除";
            this.删除ToolStripMenuItem.Click += new System.EventHandler(this.删除ToolStripMenuItem_Click);
            // 
            // FileDialogFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.BorderWidth = 0;
            this.CaptionColor = System.Drawing.Color.WhiteSmoke;
            this.CaptionShowMode = DSkin.TextShowModes.None;
            this.ClientSize = new System.Drawing.Size(1068, 712);
            this.CloseBox.BorderHoverColor = System.Drawing.Color.GreenYellow;
            this.CloseBox.BorderNormalColor = System.Drawing.Color.Transparent;
            this.CloseBox.BorderPressColor = System.Drawing.Color.GreenYellow;
            this.CloseBox.HoverBackColor = System.Drawing.Color.GreenYellow;
            this.CloseBox.NormalBackColor = System.Drawing.Color.Red;
            this.CloseBox.NormalColor = System.Drawing.Color.White;
            this.CloseBox.PressBackColor = System.Drawing.Color.GreenYellow;
            this.Controls.Add(this.dSkinLabel2);
            this.Controls.Add(this.fileContent1);
            this.Controls.Add(this.dSkinPanel1);
            this.EnableAnimation = false;
            this.MaxBox.BorderHoverColor = System.Drawing.Color.GreenYellow;
            this.MaxBox.BorderPressColor = System.Drawing.Color.GreenYellow;
            this.MaxBox.HoverBackColor = System.Drawing.Color.GreenYellow;
            this.MaxBox.PressBackColor = System.Drawing.Color.GreenYellow;
            this.MinBox.BorderHoverColor = System.Drawing.Color.GreenYellow;
            this.MinBox.BorderPressColor = System.Drawing.Color.GreenYellow;
            this.MinBox.HoverBackColor = System.Drawing.Color.GreenYellow;
            this.MinBox.NormalColor = System.Drawing.Color.White;
            this.MinBox.PressBackColor = System.Drawing.Color.GreenYellow;
            this.MinimizeBox = false;
            this.Name = "FileDialogFrm";
            this.NormalBox.BorderHoverColor = System.Drawing.Color.GreenYellow;
            this.NormalBox.BorderPressColor = System.Drawing.Color.GreenYellow;
            this.NormalBox.HoverBackColor = System.Drawing.Color.GreenYellow;
            this.NormalBox.NormalColor = System.Drawing.Color.White;
            this.NormalBox.PressBackColor = System.Drawing.Color.GreenYellow;
            this.Opacity = 0.98D;
            this.ShowInTaskbar = false;
            this.ShowShadow = true;
            this.Text = "FileDialogFrm";
            this.Load += new System.EventHandler(this.FileDialogFrm_Load);
            this.dSkinPanel1.ResumeLayout(false);
            this.dSkinPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lstLeft)).EndInit();
            this.dSkinPanel2.ResumeLayout(false);
            this.dSkinPanel3.ResumeLayout(false);
            this.dSkinPanel3.PerformLayout();
            this.dSkinContextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DSkin.Controls.DSkinPanel dSkinPanel1;
        private DSkin.Controls.DSkinPanel dSkinPanel3;
        private DSkin.Controls.DSkinPanel dSkinPanel2;
        private DSkin.Controls.DSkinLabel dSkinLabel1;
        private DSkin.Controls.DSkinButton btnCustomLocation;
        private DSkin.DirectUI.DuiIcon duiIcon1;
        private DSkin.Controls.DSkinPanel dSkinPanel4;
        private FileContent fileContent1;
        private DSkin.Controls.DSkinLabel dSkinLabel2;
        private DSkin.Controls.DSkinListBox lstLeft;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.ToolStripMenuItem 重命名ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 删除ToolStripMenuItem;
        public DSkin.Controls.DSkinContextMenuStrip dSkinContextMenuStrip1;
    }
}