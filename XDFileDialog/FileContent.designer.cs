﻿namespace XDFileDialog
{
    partial class FileContent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FileContent));
            DSkin.Controls.DSkinGridListColumn dSkinGridListColumn1 = new DSkin.Controls.DSkinGridListColumn();
            DSkin.Controls.DSkinGridListColumn dSkinGridListColumn2 = new DSkin.Controls.DSkinGridListColumn();
            DSkin.Controls.DSkinGridListColumn dSkinGridListColumn3 = new DSkin.Controls.DSkinGridListColumn();
            DSkin.Controls.DSkinGridListColumn dSkinGridListColumn4 = new DSkin.Controls.DSkinGridListColumn();
            DSkin.Controls.DSkinGridListColumn dSkinGridListColumn5 = new DSkin.Controls.DSkinGridListColumn();
            this.duiBaseControl1 = new DSkin.Controls.DSkinBaseControl();
            this.duiLabel2 = new DSkin.DirectUI.DuiLabel();
            this.cboRange = new DSkin.DirectUI.DuiComboBox();
            this.txtQuery = new DSkin.DirectUI.DuiTextBox();
            this.btnQuery = new DSkin.DirectUI.DuiIcon();
            this.duiLabel3 = new DSkin.DirectUI.DuiLabel();
            this.txtPath = new DSkin.DirectUI.DuiTextBox();
            this.dSkinListBox1 = new DSkin.Controls.DSkinListBox();
            this.dSkinPanel1 = new DSkin.Controls.DSkinPanel();
            this.btnClose = new DSkin.Controls.DSkinButton();
            this.btnOpen = new DSkin.Controls.DSkinButton();
            this.txtFileName = new DSkin.Controls.DSkinTextBox();
            this.cboFileType = new DSkin.Controls.DSkinComboBox();
            this.dSkinLabel2 = new DSkin.Controls.DSkinLabel();
            this.dSkinLabel1 = new DSkin.Controls.DSkinLabel();
            this.dSkinPanel2 = new DSkin.Controls.DSkinPanel();
            this.dSkinGridList1 = new DSkin.Controls.DSkinGridList();
            ((System.ComponentModel.ISupportInitialize)(this.cboRange.InnerListBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSkinListBox1)).BeginInit();
            this.dSkinPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboFileType.InnerListBox)).BeginInit();
            this.dSkinPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dSkinGridList1)).BeginInit();
            this.SuspendLayout();
            // 
            // duiBaseControl1
            // 
            this.duiBaseControl1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.duiBaseControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.duiBaseControl1.DUIControls.AddRange(new DSkin.DirectUI.DuiBaseControl[] {
            this.duiLabel2,
            this.cboRange,
            this.txtQuery,
            this.duiLabel3,
            this.txtPath});
            this.duiBaseControl1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.duiBaseControl1.Location = new System.Drawing.Point(0, 0);
            this.duiBaseControl1.Name = "duiBaseControl1";
            this.duiBaseControl1.Size = new System.Drawing.Size(891, 48);
            this.duiBaseControl1.TabIndex = 0;
            // 
            // duiLabel2
            // 
            this.duiLabel2.AutoSize = true;
            this.duiLabel2.DesignModeCanResize = false;
            this.duiLabel2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.duiLabel2.Location = new System.Drawing.Point(14, 2);
            this.duiLabel2.Name = "duiLabel2";
            this.duiLabel2.Size = new System.Drawing.Size(66, 18);
            this.duiLabel2.Text = "选择范围：";
            // 
            // cboRange
            // 
            this.cboRange.AdaptImage = true;
            this.cboRange.ButtonBorderColor = System.Drawing.Color.DarkGray;
            this.cboRange.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cboRange.HoverColor = System.Drawing.Color.Empty;
            // 
            // 
            // 
            this.cboRange.InnerListBox.BackColor = System.Drawing.Color.Transparent;
            this.cboRange.InnerListBox.Location = new System.Drawing.Point(0, 0);
            this.cboRange.InnerListBox.Name = "";
            this.cboRange.InnerListBox.ScrollBarWidth = 12;
            this.cboRange.InnerListBox.TabIndex = 0;
            this.cboRange.InnerListBox.Value = 0D;
            this.cboRange.IsDrawText = false;
            this.cboRange.IsPureColor = true;
            this.cboRange.Location = new System.Drawing.Point(81, 1);
            this.cboRange.Name = "cboRange";
            this.cboRange.PressColor = System.Drawing.Color.Empty;
            this.cboRange.Size = new System.Drawing.Size(157, 19);
            // 
            // 
            // 
            this.cboRange.ToolStripDropDown.BorderColor = System.Drawing.Color.DarkGray;
            this.cboRange.ToolStripDropDown.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
            this.cboRange.ToolStripDropDown.MinimumSize = new System.Drawing.Size(18, 18);
            this.cboRange.ToolStripDropDown.Name = "";
            this.cboRange.ToolStripDropDown.Padding = new System.Windows.Forms.Padding(0);
            this.cboRange.ToolStripDropDown.Resizable = false;
            this.cboRange.ToolStripDropDown.ResizeGridColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
            this.cboRange.ToolStripDropDown.ResizeGridSize = new System.Drawing.Size(16, 16);
            this.cboRange.ToolStripDropDown.Size = new System.Drawing.Size(18, 18);
            this.cboRange.ToolStripDropDown.WhereIsResizeGrid = DSkin.ResizeGridLocation.BottomRight;
            // 
            // txtQuery
            // 
            this.txtQuery.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtQuery.AutoSize = false;
            this.txtQuery.BackColor = System.Drawing.Color.White;
            this.txtQuery.Borders.AllColor = System.Drawing.Color.DarkGray;
            this.txtQuery.Borders.BottomColor = System.Drawing.Color.DarkGray;
            this.txtQuery.Borders.LeftColor = System.Drawing.Color.DarkGray;
            this.txtQuery.Borders.RightColor = System.Drawing.Color.DarkGray;
            this.txtQuery.Borders.TopColor = System.Drawing.Color.DarkGray;
            this.txtQuery.Controls.AddRange(new DSkin.DirectUI.DuiBaseControl[] {
            this.txtQuery.InnerScrollBar,
            this.btnQuery});
            this.txtQuery.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtQuery.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtQuery.Location = new System.Drawing.Point(748, 2);
            this.txtQuery.Name = "txtQuery";
            this.txtQuery.Size = new System.Drawing.Size(133, 21);
            // 
            // btnQuery
            // 
            this.btnQuery.DesignModeCanResize = false;
            this.btnQuery.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btnQuery.Icon = DSkin.Common.FontAwesomeChars.icon_search;
            this.btnQuery.Location = new System.Drawing.Point(110, 2);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(21, 18);
            // 
            // duiLabel3
            // 
            this.duiLabel3.AutoSize = true;
            this.duiLabel3.DesignModeCanResize = false;
            this.duiLabel3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.duiLabel3.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.duiLabel3.Location = new System.Drawing.Point(13, 24);
            this.duiLabel3.Name = "duiLabel3";
            this.duiLabel3.Size = new System.Drawing.Size(66, 18);
            this.duiLabel3.Text = "当前路径：";
            // 
            // txtPath
            // 
            this.txtPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPath.AutoSize = false;
            this.txtPath.Controls.AddRange(new DSkin.DirectUI.DuiBaseControl[] {
            this.txtPath.InnerScrollBar});
            this.txtPath.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtPath.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtPath.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.txtPath.Location = new System.Drawing.Point(81, 25);
            this.txtPath.Name = "txtPath";
            this.txtPath.ReadOnly = true;
            this.txtPath.Size = new System.Drawing.Size(801, 16);
            this.txtPath.Text = "D:\\";
            // 
            // dSkinListBox1
            // 
            this.dSkinListBox1.BackColor = System.Drawing.Color.White;
            this.dSkinListBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dSkinListBox1.Location = new System.Drawing.Point(0, 0);
            this.dSkinListBox1.Name = "dSkinListBox1";
            this.dSkinListBox1.ScrollBarWidth = 12;
            this.dSkinListBox1.Size = new System.Drawing.Size(891, 326);
            this.dSkinListBox1.TabIndex = 1;
            this.dSkinListBox1.Text = "dSkinListBox1";
            this.dSkinListBox1.Value = 0D;
            this.dSkinListBox1.AcceptTask += new System.EventHandler<DSkin.DirectUI.AcceptTaskEventArgs>(this.dSkinListBox1_AcceptTask);
            // 
            // dSkinPanel1
            // 
            this.dSkinPanel1.BackColor = System.Drawing.Color.White;
            this.dSkinPanel1.Borders.TopColor = System.Drawing.Color.LightGray;
            this.dSkinPanel1.Controls.Add(this.btnClose);
            this.dSkinPanel1.Controls.Add(this.btnOpen);
            this.dSkinPanel1.Controls.Add(this.txtFileName);
            this.dSkinPanel1.Controls.Add(this.cboFileType);
            this.dSkinPanel1.Controls.Add(this.dSkinLabel2);
            this.dSkinPanel1.Controls.Add(this.dSkinLabel1);
            this.dSkinPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dSkinPanel1.Location = new System.Drawing.Point(0, 374);
            this.dSkinPanel1.Name = "dSkinPanel1";
            this.dSkinPanel1.RightBottom = ((System.Drawing.Image)(resources.GetObject("dSkinPanel1.RightBottom")));
            this.dSkinPanel1.Size = new System.Drawing.Size(891, 72);
            this.dSkinPanel1.TabIndex = 2;
            this.dSkinPanel1.Text = "dSkinPanel1";
            // 
            // btnClose
            // 
            this.btnClose.AdaptImage = true;
            this.btnClose.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(227)))), ((int)(((byte)(231)))));
            this.btnClose.ButtonBorderColor = System.Drawing.Color.Gray;
            this.btnClose.ButtonBorderWidth = 1;
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnClose.ForeColor = System.Drawing.Color.Black;
            this.btnClose.HoverColor = System.Drawing.Color.Empty;
            this.btnClose.HoverImage = null;
            this.btnClose.IsPureColor = true;
            this.btnClose.Location = new System.Drawing.Point(777, 37);
            this.btnClose.Name = "btnClose";
            this.btnClose.NormalImage = null;
            this.btnClose.PressColor = System.Drawing.Color.Empty;
            this.btnClose.PressedImage = null;
            this.btnClose.Radius = 0;
            this.btnClose.ShowButtonBorder = true;
            this.btnClose.Size = new System.Drawing.Size(82, 21);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "取消";
            this.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnClose.TextPadding = 0;
            this.btnClose.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOpen
            // 
            this.btnOpen.AdaptImage = true;
            this.btnOpen.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(151)))), ((int)(((byte)(207)))));
            this.btnOpen.ButtonBorderColor = System.Drawing.Color.Gray;
            this.btnOpen.ButtonBorderWidth = 1;
            this.btnOpen.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOpen.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnOpen.ForeColor = System.Drawing.Color.White;
            this.btnOpen.HoverColor = System.Drawing.Color.Empty;
            this.btnOpen.HoverImage = null;
            this.btnOpen.IsPureColor = true;
            this.btnOpen.Location = new System.Drawing.Point(777, 13);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.NormalImage = null;
            this.btnOpen.PressColor = System.Drawing.Color.Empty;
            this.btnOpen.PressedImage = null;
            this.btnOpen.Radius = 0;
            this.btnOpen.ShowButtonBorder = true;
            this.btnOpen.Size = new System.Drawing.Size(82, 21);
            this.btnOpen.TabIndex = 3;
            this.btnOpen.Text = "打开";
            this.btnOpen.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnOpen.TextPadding = 0;
            this.btnOpen.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // txtFileName
            // 
            this.txtFileName.BitmapCache = false;
            this.txtFileName.Location = new System.Drawing.Point(85, 13);
            this.txtFileName.Name = "txtFileName";
            this.txtFileName.Size = new System.Drawing.Size(670, 21);
            this.txtFileName.TabIndex = 2;
            this.txtFileName.TransparencyKey = System.Drawing.Color.Empty;
            this.txtFileName.WaterFont = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtFileName.WaterText = "";
            this.txtFileName.WaterTextOffset = new System.Drawing.Point(0, 0);
            // 
            // cboFileType
            // 
            this.cboFileType.AdaptImage = true;
            this.cboFileType.AutoDrawSelecedItem = true;
            this.cboFileType.BaseColor = System.Drawing.Color.White;
            this.cboFileType.ButtonBorderColor = System.Drawing.Color.Gray;
            this.cboFileType.ButtonBorderWidth = 1;
            this.cboFileType.DialogResult = System.Windows.Forms.DialogResult.None;
            this.cboFileType.HoverColor = System.Drawing.Color.Empty;
            this.cboFileType.HoverImage = null;
            // 
            // 
            // 
            this.cboFileType.InnerListBox.BackColor = System.Drawing.Color.Transparent;
            this.cboFileType.InnerListBox.Location = new System.Drawing.Point(0, 0);
            this.cboFileType.InnerListBox.Name = "";
            this.cboFileType.InnerListBox.ScrollBarWidth = 12;
            this.cboFileType.InnerListBox.TabIndex = 0;
            this.cboFileType.InnerListBox.Value = 0D;
            this.cboFileType.IsDrawText = false;
            this.cboFileType.IsPureColor = true;
            this.cboFileType.ItemHoverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.cboFileType.Location = new System.Drawing.Point(85, 37);
            this.cboFileType.Name = "cboFileType";
            this.cboFileType.NormalImage = null;
            this.cboFileType.PaddingLeft = 2;
            this.cboFileType.PressColor = System.Drawing.Color.Empty;
            this.cboFileType.PressedImage = null;
            this.cboFileType.Radius = 0;
            this.cboFileType.SelectedIndex = -1;
            this.cboFileType.ShowArrow = true;
            this.cboFileType.ShowButtonBorder = true;
            this.cboFileType.Size = new System.Drawing.Size(670, 21);
            this.cboFileType.TabIndex = 1;
            this.cboFileType.Text = "dSkinComboBox1";
            this.cboFileType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cboFileType.TextPadding = 3;
            this.cboFileType.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            // 
            // 
            // 
            this.cboFileType.ToolStripDropDown.BorderColor = System.Drawing.Color.DarkGray;
            this.cboFileType.ToolStripDropDown.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
            this.cboFileType.ToolStripDropDown.MinimumSize = new System.Drawing.Size(18, 18);
            this.cboFileType.ToolStripDropDown.Name = "";
            this.cboFileType.ToolStripDropDown.Padding = new System.Windows.Forms.Padding(0);
            this.cboFileType.ToolStripDropDown.Resizable = false;
            this.cboFileType.ToolStripDropDown.ResizeGridColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
            this.cboFileType.ToolStripDropDown.ResizeGridSize = new System.Drawing.Size(16, 16);
            this.cboFileType.ToolStripDropDown.Size = new System.Drawing.Size(18, 18);
            this.cboFileType.ToolStripDropDown.WhereIsResizeGrid = DSkin.ResizeGridLocation.BottomRight;
            this.cboFileType.SelectedIndexChanged += new System.EventHandler(this.cboFileType_SelectedIndexChanged);
            // 
            // dSkinLabel2
            // 
            this.dSkinLabel2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dSkinLabel2.Location = new System.Drawing.Point(13, 40);
            this.dSkinLabel2.Name = "dSkinLabel2";
            this.dSkinLabel2.Size = new System.Drawing.Size(66, 18);
            this.dSkinLabel2.TabIndex = 0;
            this.dSkinLabel2.Text = "文件类型：";
            // 
            // dSkinLabel1
            // 
            this.dSkinLabel1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dSkinLabel1.Location = new System.Drawing.Point(25, 16);
            this.dSkinLabel1.Name = "dSkinLabel1";
            this.dSkinLabel1.Size = new System.Drawing.Size(54, 18);
            this.dSkinLabel1.TabIndex = 0;
            this.dSkinLabel1.Text = "文件名：";
            // 
            // dSkinPanel2
            // 
            this.dSkinPanel2.BackColor = System.Drawing.Color.Transparent;
            this.dSkinPanel2.Controls.Add(this.dSkinGridList1);
            this.dSkinPanel2.Controls.Add(this.dSkinListBox1);
            this.dSkinPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dSkinPanel2.Location = new System.Drawing.Point(0, 48);
            this.dSkinPanel2.Name = "dSkinPanel2";
            this.dSkinPanel2.RightBottom = ((System.Drawing.Image)(resources.GetObject("dSkinPanel2.RightBottom")));
            this.dSkinPanel2.Size = new System.Drawing.Size(891, 326);
            this.dSkinPanel2.TabIndex = 3;
            this.dSkinPanel2.Text = "dSkinPanel2";
            // 
            // dSkinGridList1
            // 
            // 
            // 
            // 
            this.dSkinGridList1.BackPageButton.AdaptImage = true;
            this.dSkinGridList1.BackPageButton.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(239)))));
            this.dSkinGridList1.BackPageButton.ButtonBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(219)))), ((int)(((byte)(219)))));
            this.dSkinGridList1.BackPageButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dSkinGridList1.BackPageButton.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dSkinGridList1.BackPageButton.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(219)))), ((int)(((byte)(219)))));
            this.dSkinGridList1.BackPageButton.IsPureColor = true;
            this.dSkinGridList1.BackPageButton.Location = new System.Drawing.Point(735, 4);
            this.dSkinGridList1.BackPageButton.Name = "BtnBackPage";
            this.dSkinGridList1.BackPageButton.PressColor = System.Drawing.Color.FromArgb(((int)(((byte)(179)))), ((int)(((byte)(179)))), ((int)(((byte)(179)))));
            this.dSkinGridList1.BackPageButton.Radius = 0;
            this.dSkinGridList1.BackPageButton.Size = new System.Drawing.Size(50, 24);
            this.dSkinGridList1.BackPageButton.Text = "上一页";
            this.dSkinGridList1.BackPageButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.dSkinGridList1.BackPageButton.TextRenderMode = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
            this.dSkinGridList1.Borders.AllColor = System.Drawing.Color.Silver;
            this.dSkinGridList1.Borders.BottomColor = System.Drawing.Color.Silver;
            this.dSkinGridList1.Borders.LeftColor = System.Drawing.Color.Silver;
            this.dSkinGridList1.Borders.RightColor = System.Drawing.Color.Silver;
            this.dSkinGridList1.Borders.TopColor = System.Drawing.Color.Silver;
            this.dSkinGridList1.ColumnHeight = 30;
            dSkinGridListColumn1.DataPropertyName = "Icon";
            // 
            // 
            // 
            dSkinGridListColumn1.Item.Font = new System.Drawing.Font("微软雅黑", 9F);
            dSkinGridListColumn1.Item.ForeColor = System.Drawing.Color.Black;
            dSkinGridListColumn1.Item.InheritanceSize = new System.Drawing.SizeF(0F, 1F);
            dSkinGridListColumn1.Item.Location = new System.Drawing.Point(0, 0);
            dSkinGridListColumn1.Item.Name = "";
            dSkinGridListColumn1.Item.Size = new System.Drawing.Size(40, 30);
            dSkinGridListColumn1.Item.Text = " ";
            dSkinGridListColumn1.Item.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            dSkinGridListColumn1.ItemType = DSkin.Controls.ControlType.DuiPictureBox;
            dSkinGridListColumn1.Name = " ";
            dSkinGridListColumn1.Visble = true;
            dSkinGridListColumn1.Width = 40;
            dSkinGridListColumn2.DataPropertyName = "Name";
            dSkinGridListColumn2.DockStyle = System.Windows.Forms.DockStyle.Left;
            // 
            // 
            // 
            dSkinGridListColumn2.Item.Font = new System.Drawing.Font("微软雅黑", 9F);
            dSkinGridListColumn2.Item.ForeColor = System.Drawing.Color.Black;
            dSkinGridListColumn2.Item.InheritanceSize = new System.Drawing.SizeF(0F, 1F);
            dSkinGridListColumn2.Item.Location = new System.Drawing.Point(40, 0);
            dSkinGridListColumn2.Item.Name = "";
            dSkinGridListColumn2.Item.Size = new System.Drawing.Size(300, 30);
            dSkinGridListColumn2.Item.Text = "名称";
            dSkinGridListColumn2.Item.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            dSkinGridListColumn2.Name = "名称";
            dSkinGridListColumn2.Visble = true;
            dSkinGridListColumn2.Width = 300;
            dSkinGridListColumn3.DataPropertyName = "LastUpdateTime";
            dSkinGridListColumn3.DockStyle = System.Windows.Forms.DockStyle.Left;
            // 
            // 
            // 
            dSkinGridListColumn3.Item.Font = new System.Drawing.Font("微软雅黑", 9F);
            dSkinGridListColumn3.Item.ForeColor = System.Drawing.Color.Black;
            dSkinGridListColumn3.Item.InheritanceSize = new System.Drawing.SizeF(0F, 1F);
            dSkinGridListColumn3.Item.Location = new System.Drawing.Point(340, 0);
            dSkinGridListColumn3.Item.Name = "";
            dSkinGridListColumn3.Item.Size = new System.Drawing.Size(140, 30);
            dSkinGridListColumn3.Item.Text = "修改时间";
            dSkinGridListColumn3.Item.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            dSkinGridListColumn3.Name = "修改时间";
            dSkinGridListColumn3.Visble = true;
            dSkinGridListColumn3.Width = 140;
            dSkinGridListColumn4.DataPropertyName = "Type";
            dSkinGridListColumn4.DockStyle = System.Windows.Forms.DockStyle.Left;
            // 
            // 
            // 
            dSkinGridListColumn4.Item.Font = new System.Drawing.Font("微软雅黑", 9F);
            dSkinGridListColumn4.Item.ForeColor = System.Drawing.Color.Black;
            dSkinGridListColumn4.Item.InheritanceSize = new System.Drawing.SizeF(0F, 1F);
            dSkinGridListColumn4.Item.Location = new System.Drawing.Point(480, 0);
            dSkinGridListColumn4.Item.Name = "";
            dSkinGridListColumn4.Item.Size = new System.Drawing.Size(150, 30);
            dSkinGridListColumn4.Item.Text = "类型";
            dSkinGridListColumn4.Item.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            dSkinGridListColumn4.Name = "类型";
            dSkinGridListColumn4.Visble = true;
            dSkinGridListColumn4.Width = 150;
            dSkinGridListColumn5.DataPropertyName = "Size";
            dSkinGridListColumn5.DockStyle = System.Windows.Forms.DockStyle.Right;
            // 
            // 
            // 
            dSkinGridListColumn5.Item.Font = new System.Drawing.Font("微软雅黑", 9F);
            dSkinGridListColumn5.Item.ForeColor = System.Drawing.Color.Black;
            dSkinGridListColumn5.Item.InheritanceSize = new System.Drawing.SizeF(0F, 1F);
            dSkinGridListColumn5.Item.Location = new System.Drawing.Point(630, 0);
            dSkinGridListColumn5.Item.Name = "";
            dSkinGridListColumn5.Item.Size = new System.Drawing.Size(90, 30);
            dSkinGridListColumn5.Item.Text = "大小";
            dSkinGridListColumn5.Item.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            dSkinGridListColumn5.Name = "大小";
            dSkinGridListColumn5.Visble = true;
            dSkinGridListColumn5.Width = 90;
            this.dSkinGridList1.Columns.AddRange(new DSkin.Controls.DSkinGridListColumn[] {
            dSkinGridListColumn1,
            dSkinGridListColumn2,
            dSkinGridListColumn3,
            dSkinGridListColumn4,
            dSkinGridListColumn5});
            this.dSkinGridList1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dSkinGridList1.DoubleItemsBackColor = System.Drawing.Color.White;
            this.dSkinGridList1.EnabledOrder = false;
            this.dSkinGridList1.EnablePage = false;
            // 
            // 
            // 
            this.dSkinGridList1.FirstPageButton.AdaptImage = true;
            this.dSkinGridList1.FirstPageButton.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(239)))));
            this.dSkinGridList1.FirstPageButton.ButtonBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(219)))), ((int)(((byte)(219)))));
            this.dSkinGridList1.FirstPageButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dSkinGridList1.FirstPageButton.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dSkinGridList1.FirstPageButton.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(219)))), ((int)(((byte)(219)))));
            this.dSkinGridList1.FirstPageButton.IsPureColor = true;
            this.dSkinGridList1.FirstPageButton.Location = new System.Drawing.Point(687, 4);
            this.dSkinGridList1.FirstPageButton.Name = "BtnFistPage";
            this.dSkinGridList1.FirstPageButton.PressColor = System.Drawing.Color.FromArgb(((int)(((byte)(179)))), ((int)(((byte)(179)))), ((int)(((byte)(179)))));
            this.dSkinGridList1.FirstPageButton.Radius = 0;
            this.dSkinGridList1.FirstPageButton.Size = new System.Drawing.Size(44, 24);
            this.dSkinGridList1.FirstPageButton.Text = "首页";
            this.dSkinGridList1.FirstPageButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.dSkinGridList1.FirstPageButton.TextRenderMode = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
            // 
            // 
            // 
            this.dSkinGridList1.GoPageButton.AdaptImage = true;
            this.dSkinGridList1.GoPageButton.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(239)))));
            this.dSkinGridList1.GoPageButton.ButtonBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.dSkinGridList1.GoPageButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dSkinGridList1.GoPageButton.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dSkinGridList1.GoPageButton.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(219)))), ((int)(((byte)(219)))));
            this.dSkinGridList1.GoPageButton.IsPureColor = true;
            this.dSkinGridList1.GoPageButton.Location = new System.Drawing.Point(290, 4);
            this.dSkinGridList1.GoPageButton.Name = "BtnGoPage";
            this.dSkinGridList1.GoPageButton.PressColor = System.Drawing.Color.FromArgb(((int)(((byte)(179)))), ((int)(((byte)(179)))), ((int)(((byte)(179)))));
            this.dSkinGridList1.GoPageButton.Radius = 0;
            this.dSkinGridList1.GoPageButton.Size = new System.Drawing.Size(44, 24);
            this.dSkinGridList1.GoPageButton.Text = "跳转";
            this.dSkinGridList1.GoPageButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.dSkinGridList1.GoPageButton.TextRenderMode = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
            this.dSkinGridList1.GridLineColor = System.Drawing.Color.Silver;
            this.dSkinGridList1.HeaderFont = new System.Drawing.Font("微软雅黑", 9F);
            // 
            // 
            // 
            this.dSkinGridList1.HScrollBar.AutoSize = false;
            this.dSkinGridList1.HScrollBar.Fillet = true;
            this.dSkinGridList1.HScrollBar.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dSkinGridList1.HScrollBar.Location = new System.Drawing.Point(0, 202);
            this.dSkinGridList1.HScrollBar.Maximum = 46;
            this.dSkinGridList1.HScrollBar.Name = "";
            this.dSkinGridList1.HScrollBar.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.dSkinGridList1.HScrollBar.ScrollBarLenght = 369;
            this.dSkinGridList1.HScrollBar.ScrollBarPartitionWidth = new System.Windows.Forms.Padding(5);
            this.dSkinGridList1.HScrollBar.Size = new System.Drawing.Size(891, 12);
            this.dSkinGridList1.HScrollBar.Visible = false;
            // 
            // 
            // 
            this.dSkinGridList1.LastPageButton.AdaptImage = true;
            this.dSkinGridList1.LastPageButton.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(239)))));
            this.dSkinGridList1.LastPageButton.ButtonBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(219)))), ((int)(((byte)(219)))));
            this.dSkinGridList1.LastPageButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dSkinGridList1.LastPageButton.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dSkinGridList1.LastPageButton.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(219)))), ((int)(((byte)(219)))));
            this.dSkinGridList1.LastPageButton.IsPureColor = true;
            this.dSkinGridList1.LastPageButton.Location = new System.Drawing.Point(843, 4);
            this.dSkinGridList1.LastPageButton.Name = "BtnLastPage";
            this.dSkinGridList1.LastPageButton.PressColor = System.Drawing.Color.FromArgb(((int)(((byte)(179)))), ((int)(((byte)(179)))), ((int)(((byte)(179)))));
            this.dSkinGridList1.LastPageButton.Radius = 0;
            this.dSkinGridList1.LastPageButton.Size = new System.Drawing.Size(44, 24);
            this.dSkinGridList1.LastPageButton.Text = "末页";
            this.dSkinGridList1.LastPageButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.dSkinGridList1.LastPageButton.TextRenderMode = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
            this.dSkinGridList1.Location = new System.Drawing.Point(0, 0);
            this.dSkinGridList1.Name = "dSkinGridList1";
            // 
            // 
            // 
            this.dSkinGridList1.NextPageButton.AdaptImage = true;
            this.dSkinGridList1.NextPageButton.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(239)))));
            this.dSkinGridList1.NextPageButton.ButtonBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(219)))), ((int)(((byte)(219)))));
            this.dSkinGridList1.NextPageButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dSkinGridList1.NextPageButton.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dSkinGridList1.NextPageButton.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(219)))), ((int)(((byte)(219)))));
            this.dSkinGridList1.NextPageButton.IsPureColor = true;
            this.dSkinGridList1.NextPageButton.Location = new System.Drawing.Point(789, 4);
            this.dSkinGridList1.NextPageButton.Name = "BtnNextPage";
            this.dSkinGridList1.NextPageButton.PressColor = System.Drawing.Color.FromArgb(((int)(((byte)(179)))), ((int)(((byte)(179)))), ((int)(((byte)(179)))));
            this.dSkinGridList1.NextPageButton.Radius = 0;
            this.dSkinGridList1.NextPageButton.Size = new System.Drawing.Size(50, 24);
            this.dSkinGridList1.NextPageButton.Text = "下一页";
            this.dSkinGridList1.NextPageButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.dSkinGridList1.NextPageButton.TextRenderMode = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
            this.dSkinGridList1.SelectedItem = null;
            this.dSkinGridList1.Size = new System.Drawing.Size(891, 326);
            this.dSkinGridList1.TabIndex = 2;
            this.dSkinGridList1.Text = "dSkinGridList1";
            // 
            // 
            // 
            this.dSkinGridList1.VScrollBar.AutoSize = false;
            this.dSkinGridList1.VScrollBar.BitmapCache = true;
            this.dSkinGridList1.VScrollBar.Fillet = true;
            this.dSkinGridList1.VScrollBar.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dSkinGridList1.VScrollBar.LargeChange = 1000;
            this.dSkinGridList1.VScrollBar.Location = new System.Drawing.Point(878, 1);
            this.dSkinGridList1.VScrollBar.Margin = new System.Windows.Forms.Padding(1);
            this.dSkinGridList1.VScrollBar.Maximum = 10000;
            this.dSkinGridList1.VScrollBar.Name = "";
            this.dSkinGridList1.VScrollBar.ScrollBarPartitionWidth = new System.Windows.Forms.Padding(5);
            this.dSkinGridList1.VScrollBar.ShowBorder = false;
            this.dSkinGridList1.VScrollBar.Size = new System.Drawing.Size(12, 293);
            this.dSkinGridList1.VScrollBar.SmallChange = 500;
            this.dSkinGridList1.VScrollBar.Visible = false;
            this.dSkinGridList1.ItemClick += new System.EventHandler<DSkin.Controls.DSkinGridListMouseEventArgs>(this.dSkinGridList1_ItemClick);
            this.dSkinGridList1.ItemDoubleClick += new System.EventHandler<DSkin.Controls.DSkinGridListMouseEventArgs>(this.dSkinGridList1_ItemDoubleClick);
            // 
            // FileContent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.dSkinPanel2);
            this.Controls.Add(this.dSkinPanel1);
            this.Controls.Add(this.duiBaseControl1);
            this.Margin = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.Name = "FileContent";
            this.Size = new System.Drawing.Size(891, 446);
            ((System.ComponentModel.ISupportInitialize)(this.cboRange.InnerListBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dSkinListBox1)).EndInit();
            this.dSkinPanel1.ResumeLayout(false);
            this.dSkinPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboFileType.InnerListBox)).EndInit();
            this.dSkinPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dSkinGridList1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DSkin.Controls.DSkinBaseControl duiBaseControl1;
        private DSkin.DirectUI.DuiLabel duiLabel2;
        private DSkin.DirectUI.DuiComboBox cboRange;
        private DSkin.DirectUI.DuiTextBox txtQuery;
        private DSkin.DirectUI.DuiIcon btnQuery;
        private DSkin.DirectUI.DuiLabel duiLabel3;
        private DSkin.DirectUI.DuiTextBox txtPath;
        private DSkin.Controls.DSkinListBox dSkinListBox1;
        private DSkin.Controls.DSkinPanel dSkinPanel1;
        private DSkin.Controls.DSkinPanel dSkinPanel2;
        private DSkin.Controls.DSkinLabel dSkinLabel2;
        private DSkin.Controls.DSkinLabel dSkinLabel1;
        private DSkin.Controls.DSkinButton btnClose;
        private DSkin.Controls.DSkinButton btnOpen;
        private DSkin.Controls.DSkinGridList dSkinGridList1;
        public DSkin.Controls.DSkinTextBox txtFileName;
        public DSkin.Controls.DSkinComboBox cboFileType;


    }
}