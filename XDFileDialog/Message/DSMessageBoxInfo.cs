﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace XDFileDialog.Message
{
    public partial class DSMessageBoxInfo : DSkin.Forms.Skin_VS_Light
    {
        public DSMessageBoxInfo(string msg)
        {
            InitializeComponent();
            lbtxet.Text = msg;
            SetSize(msg);
            SetButtons(MessageBoxButtons.OK);
        }
        public DSMessageBoxInfo(string msg, string caption)
        {
            InitializeComponent();
            this.Text = caption;
            lbtxet.Text = msg;
            SetSize(msg);
            SetButtons(MessageBoxButtons.OK);
        }
        public DSMessageBoxInfo(string msg, string caption,MessageBoxButtons button)
        {
            InitializeComponent();
            this.Text = caption;
            lbtxet.Text = msg;
            SetSize(msg);
            SetButtons(button);
        }
        public void SetButtons(MessageBoxButtons btns) {
            panlOK.Visible = false;
            panlOKCancel.Visible = false;
            panlYesNo.Visible = false;
            switch (btns)
            {
                case MessageBoxButtons.OK:
                    panlOK.Visible = true;
                    break;
                case MessageBoxButtons.OKCancel:
                    panlOKCancel.Visible = true;
                    break;
                case MessageBoxButtons.YesNo:
                    panlYesNo.Visible = true;
                    break;
                default:
                    break;
            }
        }
        public void SetSize(string msg)
        {
            SizeF size = TextRenderer.MeasureText(msg, new Font("微软雅黑", 10, FontStyle.Regular));
            int TempWidth = (int)size.Width;
            if (TempWidth >= 303)
            {
                this.Width = (int)size.Width + 130;
            }

            this.Height = (int)(this.Height + size.Height);
        }

        private void btn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void DSMessageBoxInfo_Load(object sender, EventArgs e)
        {

        }
    }
}
