﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace XDFileDialog.Message
{
    public static class DSMessageBox
    {
        public static DialogResult Show(string msg)
        {
            DSMessageBoxInfo from = new DSMessageBoxInfo(msg);
            return from.ShowDialog();
        }
        public static DialogResult Show(string msg, string caption)
        {
            DSMessageBoxInfo from = new DSMessageBoxInfo(msg, caption);
            return from.ShowDialog();
        }
        public static DialogResult ShowOKCancel(string msg, string caption)
        {
            DSMessageBoxInfo from = new DSMessageBoxInfo(msg, caption, MessageBoxButtons.OKCancel);
            return from.ShowDialog();
        }

        
    }
}
