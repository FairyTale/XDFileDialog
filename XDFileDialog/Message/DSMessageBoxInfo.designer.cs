﻿namespace XDFileDialog.Message
{
    partial class DSMessageBoxInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DSMessageBoxInfo));
            this.dSkinPanel1 = new DSkin.Controls.DSkinPanel();
            this.panlOK = new DSkin.Controls.DSkinPanel();
            this.btnOKOne = new DSkin.Controls.DSkinButton();
            this.panlOKCancel = new DSkin.Controls.DSkinPanel();
            this.panlYesNo = new DSkin.Controls.DSkinPanel();
            this.btnYes = new DSkin.Controls.DSkinButton();
            this.dSkinButton2 = new DSkin.Controls.DSkinButton();
            this.btnOK = new DSkin.Controls.DSkinButton();
            this.btnClose = new DSkin.Controls.DSkinButton();
            this.lbtxet = new DSkin.Controls.DSkinLabel();
            this.dSkinPictureBox1 = new DSkin.Controls.DSkinPictureBox();
            this.dSkinPanel1.SuspendLayout();
            this.panlOK.SuspendLayout();
            this.panlOKCancel.SuspendLayout();
            this.panlYesNo.SuspendLayout();
            this.SuspendLayout();
            // 
            // dSkinPanel1
            // 
            this.dSkinPanel1.BackColor = System.Drawing.Color.White;
            this.dSkinPanel1.Controls.Add(this.panlOKCancel);
            this.dSkinPanel1.Controls.Add(this.lbtxet);
            this.dSkinPanel1.Controls.Add(this.dSkinPictureBox1);
            this.dSkinPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dSkinPanel1.Location = new System.Drawing.Point(4, 34);
            this.dSkinPanel1.Name = "dSkinPanel1";
            this.dSkinPanel1.RightBottom = ((System.Drawing.Image)(resources.GetObject("dSkinPanel1.RightBottom")));
            this.dSkinPanel1.Size = new System.Drawing.Size(395, 132);
            this.dSkinPanel1.TabIndex = 0;
            this.dSkinPanel1.Text = "dSkinPanel1";
            // 
            // panlOK
            // 
            this.panlOK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.panlOK.BackColor = System.Drawing.Color.Transparent;
            this.panlOK.Controls.Add(this.btnOKOne);
            this.panlOK.Location = new System.Drawing.Point(0, 0);
            this.panlOK.Name = "panlOK";
            this.panlOK.RightBottom = ((System.Drawing.Image)(resources.GetObject("panlOK.RightBottom")));
            this.panlOK.Size = new System.Drawing.Size(284, 46);
            this.panlOK.TabIndex = 9;
            this.panlOK.Text = "dSkinPanel2";
            // 
            // btnOKOne
            // 
            this.btnOKOne.AdaptImage = true;
            this.btnOKOne.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnOKOne.BaseColor = System.Drawing.Color.SteelBlue;
            this.btnOKOne.Borders.AllWidth = 0;
            this.btnOKOne.Borders.BottomWidth = 0;
            this.btnOKOne.Borders.LeftWidth = 0;
            this.btnOKOne.Borders.RightWidth = 0;
            this.btnOKOne.Borders.TopWidth = 0;
            this.btnOKOne.ButtonBorderColor = System.Drawing.Color.Gray;
            this.btnOKOne.ButtonBorderWidth = 0;
            this.btnOKOne.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOKOne.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnOKOne.ForeColor = System.Drawing.Color.White;
            this.btnOKOne.HoverColor = System.Drawing.Color.Empty;
            this.btnOKOne.HoverImage = null;
            this.btnOKOne.IsPureColor = true;
            this.btnOKOne.Location = new System.Drawing.Point(82, 3);
            this.btnOKOne.Name = "btnOKOne";
            this.btnOKOne.NormalImage = null;
            this.btnOKOne.PressColor = System.Drawing.Color.Empty;
            this.btnOKOne.PressedImage = null;
            this.btnOKOne.Radius = 0;
            this.btnOKOne.ShowButtonBorder = true;
            this.btnOKOne.Size = new System.Drawing.Size(113, 40);
            this.btnOKOne.TabIndex = 5;
            this.btnOKOne.Text = "是";
            this.btnOKOne.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnOKOne.TextPadding = 0;
            this.btnOKOne.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.btnOKOne.Click += new System.EventHandler(this.btn_Click);
            // 
            // panlOKCancel
            // 
            this.panlOKCancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.panlOKCancel.BackColor = System.Drawing.Color.Transparent;
            this.panlOKCancel.Controls.Add(this.panlYesNo);
            this.panlOKCancel.Controls.Add(this.btnOK);
            this.panlOKCancel.Controls.Add(this.btnClose);
            this.panlOKCancel.Location = new System.Drawing.Point(56, 71);
            this.panlOKCancel.Name = "panlOKCancel";
            this.panlOKCancel.RightBottom = ((System.Drawing.Image)(resources.GetObject("panlOKCancel.RightBottom")));
            this.panlOKCancel.Size = new System.Drawing.Size(284, 46);
            this.panlOKCancel.TabIndex = 7;
            this.panlOKCancel.Text = "dSkinPanel2";
            // 
            // panlYesNo
            // 
            this.panlYesNo.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.panlYesNo.BackColor = System.Drawing.Color.Transparent;
            this.panlYesNo.Controls.Add(this.panlOK);
            this.panlYesNo.Controls.Add(this.btnYes);
            this.panlYesNo.Controls.Add(this.dSkinButton2);
            this.panlYesNo.Location = new System.Drawing.Point(0, 0);
            this.panlYesNo.Name = "panlYesNo";
            this.panlYesNo.RightBottom = ((System.Drawing.Image)(resources.GetObject("panlYesNo.RightBottom")));
            this.panlYesNo.Size = new System.Drawing.Size(284, 46);
            this.panlYesNo.TabIndex = 8;
            this.panlYesNo.Text = "dSkinPanel2";
            // 
            // btnYes
            // 
            this.btnYes.AdaptImage = true;
            this.btnYes.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnYes.BaseColor = System.Drawing.Color.SteelBlue;
            this.btnYes.Borders.AllWidth = 0;
            this.btnYes.Borders.BottomWidth = 0;
            this.btnYes.Borders.LeftWidth = 0;
            this.btnYes.Borders.RightWidth = 0;
            this.btnYes.Borders.TopWidth = 0;
            this.btnYes.ButtonBorderColor = System.Drawing.Color.Gray;
            this.btnYes.ButtonBorderWidth = 0;
            this.btnYes.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.btnYes.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnYes.ForeColor = System.Drawing.Color.White;
            this.btnYes.HoverColor = System.Drawing.Color.Empty;
            this.btnYes.HoverImage = null;
            this.btnYes.IsPureColor = true;
            this.btnYes.Location = new System.Drawing.Point(3, 3);
            this.btnYes.Name = "btnYes";
            this.btnYes.NormalImage = null;
            this.btnYes.PressColor = System.Drawing.Color.Empty;
            this.btnYes.PressedImage = null;
            this.btnYes.Radius = 0;
            this.btnYes.ShowButtonBorder = true;
            this.btnYes.Size = new System.Drawing.Size(113, 40);
            this.btnYes.TabIndex = 5;
            this.btnYes.Text = "是";
            this.btnYes.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnYes.TextPadding = 0;
            this.btnYes.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.btnYes.Click += new System.EventHandler(this.btn_Click);
            // 
            // dSkinButton2
            // 
            this.dSkinButton2.AdaptImage = true;
            this.dSkinButton2.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.dSkinButton2.BaseColor = System.Drawing.Color.Red;
            this.dSkinButton2.Borders.AllWidth = 0;
            this.dSkinButton2.Borders.BottomWidth = 0;
            this.dSkinButton2.Borders.LeftWidth = 0;
            this.dSkinButton2.Borders.RightWidth = 0;
            this.dSkinButton2.Borders.TopWidth = 0;
            this.dSkinButton2.ButtonBorderColor = System.Drawing.Color.Gray;
            this.dSkinButton2.ButtonBorderWidth = 0;
            this.dSkinButton2.DialogResult = System.Windows.Forms.DialogResult.No;
            this.dSkinButton2.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dSkinButton2.ForeColor = System.Drawing.Color.White;
            this.dSkinButton2.HoverColor = System.Drawing.Color.Empty;
            this.dSkinButton2.HoverImage = null;
            this.dSkinButton2.IsPureColor = true;
            this.dSkinButton2.Location = new System.Drawing.Point(168, 3);
            this.dSkinButton2.Name = "dSkinButton2";
            this.dSkinButton2.NormalImage = null;
            this.dSkinButton2.PressColor = System.Drawing.Color.Empty;
            this.dSkinButton2.PressedImage = null;
            this.dSkinButton2.Radius = 0;
            this.dSkinButton2.ShowButtonBorder = true;
            this.dSkinButton2.Size = new System.Drawing.Size(113, 40);
            this.dSkinButton2.TabIndex = 4;
            this.dSkinButton2.Text = "否";
            this.dSkinButton2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.dSkinButton2.TextPadding = 0;
            this.dSkinButton2.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.dSkinButton2.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnOK
            // 
            this.btnOK.AdaptImage = true;
            this.btnOK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnOK.BaseColor = System.Drawing.Color.SteelBlue;
            this.btnOK.Borders.AllWidth = 0;
            this.btnOK.Borders.BottomWidth = 0;
            this.btnOK.Borders.LeftWidth = 0;
            this.btnOK.Borders.RightWidth = 0;
            this.btnOK.Borders.TopWidth = 0;
            this.btnOK.ButtonBorderColor = System.Drawing.Color.Gray;
            this.btnOK.ButtonBorderWidth = 0;
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnOK.ForeColor = System.Drawing.Color.White;
            this.btnOK.HoverColor = System.Drawing.Color.Empty;
            this.btnOK.HoverImage = null;
            this.btnOK.IsPureColor = true;
            this.btnOK.Location = new System.Drawing.Point(3, 3);
            this.btnOK.Name = "btnOK";
            this.btnOK.NormalImage = null;
            this.btnOK.PressColor = System.Drawing.Color.Empty;
            this.btnOK.PressedImage = null;
            this.btnOK.Radius = 0;
            this.btnOK.ShowButtonBorder = true;
            this.btnOK.Size = new System.Drawing.Size(113, 40);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "确      定";
            this.btnOK.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnOK.TextPadding = 0;
            this.btnOK.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.btnOK.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnClose
            // 
            this.btnClose.AdaptImage = true;
            this.btnClose.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnClose.BaseColor = System.Drawing.Color.Red;
            this.btnClose.Borders.AllWidth = 0;
            this.btnClose.Borders.BottomWidth = 0;
            this.btnClose.Borders.LeftWidth = 0;
            this.btnClose.Borders.RightWidth = 0;
            this.btnClose.Borders.TopWidth = 0;
            this.btnClose.ButtonBorderColor = System.Drawing.Color.Gray;
            this.btnClose.ButtonBorderWidth = 0;
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.HoverColor = System.Drawing.Color.Empty;
            this.btnClose.HoverImage = null;
            this.btnClose.IsPureColor = true;
            this.btnClose.Location = new System.Drawing.Point(168, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.NormalImage = null;
            this.btnClose.PressColor = System.Drawing.Color.Empty;
            this.btnClose.PressedImage = null;
            this.btnClose.Radius = 0;
            this.btnClose.ShowButtonBorder = true;
            this.btnClose.Size = new System.Drawing.Size(113, 40);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "取      消";
            this.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnClose.TextPadding = 0;
            this.btnClose.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.btnClose.Click += new System.EventHandler(this.btn_Click);
            // 
            // lbtxet
            // 
            this.lbtxet.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.lbtxet.Location = new System.Drawing.Point(88, 35);
            this.lbtxet.Name = "lbtxet";
            this.lbtxet.Size = new System.Drawing.Size(84, 20);
            this.lbtxet.TabIndex = 6;
            this.lbtxet.Text = "dSkinLabel1";
            this.lbtxet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dSkinPictureBox1
            // 
            this.dSkinPictureBox1.Image = null;
            this.dSkinPictureBox1.Images = null;
            this.dSkinPictureBox1.Interval = 40;
            this.dSkinPictureBox1.Location = new System.Drawing.Point(19, 15);
            this.dSkinPictureBox1.Name = "dSkinPictureBox1";
            this.dSkinPictureBox1.Size = new System.Drawing.Size(50, 50);
            this.dSkinPictureBox1.TabIndex = 0;
            this.dSkinPictureBox1.Text = "dSkinPictureBox1";
            // 
            // DSMessageBoxInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SteelBlue;
            this.CaptionColor = System.Drawing.Color.White;
            this.CaptionFont = new System.Drawing.Font("微软雅黑", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ClientSize = new System.Drawing.Size(403, 170);
            this.CloseBox.HoverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(252)))), ((int)(((byte)(253)))));
            this.CloseBox.PressBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.ControlBox = false;
            this.Controls.Add(this.dSkinPanel1);
            this.DoubleClickMaximized = false;
            this.DrawIcon = false;
            this.EnableAnimation = false;
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.MaxBox.HoverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(252)))), ((int)(((byte)(253)))));
            this.MaxBox.PressBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.MinBox.HoverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(252)))), ((int)(((byte)(253)))));
            this.MinBox.PressBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.Name = "DSMessageBoxInfo";
            this.Text = "提示";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.DSMessageBoxInfo_Load);
            this.dSkinPanel1.ResumeLayout(false);
            this.dSkinPanel1.PerformLayout();
            this.panlOK.ResumeLayout(false);
            this.panlOKCancel.ResumeLayout(false);
            this.panlYesNo.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DSkin.Controls.DSkinPanel dSkinPanel1;
        private DSkin.Controls.DSkinPictureBox dSkinPictureBox1;
        private DSkin.Controls.DSkinLabel lbtxet;
        private DSkin.Controls.DSkinButton btnOK;
        private DSkin.Controls.DSkinButton btnClose;
        private DSkin.Controls.DSkinPanel panlOKCancel;
        private DSkin.Controls.DSkinPanel panlYesNo;
        private DSkin.Controls.DSkinButton btnYes;
        private DSkin.Controls.DSkinButton dSkinButton2;
        private DSkin.Controls.DSkinPanel panlOK;
        private DSkin.Controls.DSkinButton btnOKOne;
    }
}