using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Text.RegularExpressions;
using DSkin.Controls;
using XDFileDialog.Template;
using XDFileDialog.Model;
using InterFace;
using XDFileDialog.Message;

namespace XDFileDialog
{
    public partial class FileDialogFrm : XDFileDialog.FrmBase
    {
        /// <summary>
        /// 路径
        /// </summary>
        public string Path
        {
            set
            {
                fileContent1.Path = value;
            }
            get
            {
                return fileContent1.Path;
            }
        }
        /// <summary>
        /// 整个路径
        /// </summary>
        public string PathFileName
        {
            set
            {
                fileContent1.PathFileName = value;
            }
            get
            {
                return fileContent1.PathFileName;
            }
        }
        /// <summary>
        /// 文件名和扩展名
        /// </summary>
        public string SafeFileName
        {
            set
            {
                fileContent1.SafeFileName = value;
            }
            get
            {
                return fileContent1.SafeFileName;
            }
        }
        public delegate void FileOkEventArgs(object sender, string path);
        /// <summary>
        /// 获取到文件路径
        /// </summary>
        public event FileOkEventArgs FileOk;
        EditHelp<LeftModel> help = new EditHelp<LeftModel>(PathHelp.SysPach);
        public FileDialogFrm()
        {
            InitializeComponent();
        }

        private void FileDialogFrm_Load(object sender, EventArgs e)
        {
            fileContent1.GetMyComputer();
            GetLeft();
        }

        public void GetLeft()
        {
            List<LeftModel> list = new List<LeftModel>();
            list.Add(new LeftModel()
            {
                Id = 100002,
                Name = "我的文档",
                Path = SpecialSysPathHelp.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                Type = false,
            });
            list.Add(new LeftModel()
            {
                Id = 100001,
                Name = "我的电脑",
                Path = "",
                Type = false,
            });

            list.Add(new LeftModel()
            {
                Id = 100003,
                Name = "桌面",
                Path = SpecialSysPathHelp.GetFolderPath(Environment.SpecialFolder.DesktopDirectory),
                Type = false,
            });
            list.AddRange(help.List());
            lstLeft.SelectionMode = SelectionModes.Radio;
            lstLeft.Template = typeof(LeftListTemplate);
            lstLeft.DataSource = list;
        }
        public void Reset()
        {
            fileContent1.txtFileName.Text = "";
        }
        /// <summary>
        /// 获取到路径
        /// </summary>
        /// <param name="path"></param>
        public void ReturnFile(string path)
        {
            if (FileOk != null)
            {
                FileOk(this, path);
            }
        }
        /// <summary>
        /// 文件类型
        /// </summary>
        public void getFileType(string str, int index)
        {
            string[] name = str.Split('|');
            List<string> list = new List<string>();
            for (int i = 0; i < name.Length; i += 2)
            {
                fileContent1.cboFileType.AddItem(name[i]);
                list.Add(name[i + 1]);
            }
            fileContent1.cboFileType.Tag = list;
            fileContent1.cboFileType.SelectedIndex = index;
        }
        private void lstLeft_AcceptTask(object sender, DSkin.DirectUI.AcceptTaskEventArgs e)
        {
            if (e.TaskName == "我的电脑")
            {
                fileContent1.GetMyComputer();
            }
            else
            {
                fileContent1.GetCatalog(e.Data.ToString());
            }
        }

        private void btnCustomLocation_Click(object sender, EventArgs e)
        {

            folderBrowserDialog1.ShowDialog();
            LeftModel model = new LeftModel();
            model.Path = folderBrowserDialog1.SelectedPath;
            model.Type = true;
            model.Name = model.Path.Substring(model.Path.LastIndexOf('\\') + 1);
            help.Add(model);
            GetLeft();
        }

        private void 删除ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ;
            if (DSMessageBox.ShowOKCancel("是否删除此项？", "提示") == DialogResult.OK)
            {
                help.Del(lstLeft.SelectedItem.Name);
                GetLeft();
            }

        }

        private void 重命名ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmReName frm = new FrmReName();
            frm.LeftId = lstLeft.SelectedItem.Name;
            if (frm.ShowDialog() == DialogResult.OK)
            {
                GetLeft();
            }
        }


    }
}
