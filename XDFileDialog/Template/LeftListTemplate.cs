﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace XDFileDialog.Template
{
    public partial class LeftListTemplate : DSkin.Controls.DSkinListItemTemplate
    {
        public LeftListTemplate()
        {
            InitializeComponent();
        }

        public LeftListTemplate(IContainer container)
        {
            container.Add(this);
            InitializeComponent();
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            lblTitle.Text = this.Eval("Name").ToString();
            Name = this.Eval("Id").ToString();
            if (!(bool)this.Eval("Type"))
            {
                btnDetail.Visible = false;
            }
        }


        private void LeftListTemplate_IsSelectedChanged(object sender, EventArgs e)
        {
            if (this.IsSelected)
            {
                this.Borders.LeftWidth = 10;
                this.BackColor = Color.FromArgb(109, 117, 166);
                this.SendTask(lblTitle.Text.Trim(),this.Eval("Path"));

            }
            else
            {
                this.Borders.LeftWidth = 0;
                this.BackColor = Color.FromArgb(51, 114, 158);
            }
        }

        private void LeftListTemplate_MouseLeave(object sender, EventArgs e)
        {
            SetHover(false);
        }

        private void LeftListTemplate_MouseEnter(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            SetHover(true);
        }
        //是否选中
        private void SetHover(bool hover)
        {
            if (this.IsSelected)
            {
                return;
            }
            if (hover)
            {
                this.Borders.LeftWidth = 10;
                this.BackColor = Color.FromArgb(109, 117, 166);

            }
            else
            {
                this.Borders.LeftWidth = 0;
                this.BackColor = Color.FromArgb(51, 114, 158);
            }
        }



        private void btnDetail_MouseClick(object sender, DSkin.DirectUI.DuiMouseEventArgs e)
        {
            FileDialogFrm frm=(this.DSkinListBox.FindForm() as FileDialogFrm);
            int left = frm.Left+this.Left + e.X + 260;
            int top = frm.Top + this.Top + e.Y + btnDetail.Height + 60;
            frm.dSkinContextMenuStrip1.Location = new Point(left, top);
            frm.dSkinContextMenuStrip1.Show(new Point(left, top), ToolStripDropDownDirection.BelowLeft);
        }
    }
}
