﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Drawing;
using XDFileDialog.Help;

namespace XDFileDialog.Template
{
    public partial class MyComputerTemplate : DSkin.Controls.DSkinListItemTemplate
    {
        public MyComputerTemplate()
        {
            InitializeComponent();
        }

        public MyComputerTemplate(IContainer container)
        {
            container.Add(this);
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            lblTitle.Text = string.Format("{0}({1})",this.Eval("VolumeName").ToString(), this.Eval("Name").ToString());
            lblContent.Text = string.Format("{0}B可用，共{1}B", this.Eval("FreeSpace").ToString(), this.Eval("Size").ToString());
            double freeSpace = ConvertSize(this.Eval("FreeSpace").ToString());
            double size = ConvertSize(this.Eval("Size").ToString());
            pgbCapacity.Value = (int)((size - freeSpace) / size * 100);
            
            picIcon.Image = SystemIconHelp.GetDriverIcon(Convert.ToChar( this.Eval("Name").ToString().Replace(":","")), false).ToBitmap();
        }
        public double ConvertSize(string str)
        {
            string value = str.Split(' ')[0];
            string size = str.Split(' ')[1];
            switch (size)
            {
                case "K":
                    return double.Parse(value);
                    break;
                case "M":
                    return double.Parse(value) * 1024;
                    break;
                case "G":
                    return double.Parse(value) * 1024 * 1024;
                    break;
                case "T":
                    return double.Parse(value) * 1024 * 1024 * 1024;
                    break;
            }
            return 0;
        }
        private void MyComputerTemplate_MouseEnter(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            SetHover(true);
        }

        private void MyComputerTemplate_MouseLeave(object sender, EventArgs e)
        {
            SetHover(false);
        }

        //是否选中
        private void SetHover(bool hover)
        {
            if (this.IsSelected)
            {
                return;
            }
            if (hover)
            {
                this.BackColor = Color.FromArgb(203, 232, 255);
                this.Borders.AllColor = Color.FromArgb(38, 232, 218);
            }
            else
            {
                this.BackColor = Color.White;
                this.Borders.AllColor = Color.Transparent;
            }
        }

        private void MyComputerTemplate_IsSelectedChanged(object sender, EventArgs e)
        {
            if (this.IsSelected)
            {
                this.BackColor = Color.FromArgb(203, 232, 255);
                this.Borders.AllColor = Color.Transparent;
            }
            else
            {
                this.BackColor = Color.White;
                this.Borders.AllColor = Color.Transparent;
            }
        }

        private void MyComputerTemplate_MouseDoubleClick(object sender, DSkin.DirectUI.DuiMouseEventArgs e)
        {
            this.SendTask("计算机", this.Eval("Name").ToString());
        }
    }
}
