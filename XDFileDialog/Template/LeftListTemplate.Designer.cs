﻿namespace XDFileDialog.Template
{
    partial class LeftListTemplate
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDetail = new DSkin.DirectUI.DuiIcon();
            this.lblTitle = new DSkin.DirectUI.DuiLabel();
            // 
            // btnDetail
            // 
            this.btnDetail.Borders.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnDetail.Borders.LeftWidth = 0;
            this.btnDetail.DesignModeCanResize = false;
            this.btnDetail.ForeColor = System.Drawing.Color.White;
            this.btnDetail.Icon = DSkin.Common.FontAwesomeChars.icon_ellipsis_v;
            this.btnDetail.IconSize = 15F;
            this.btnDetail.Location = new System.Drawing.Point(165, 12);
            this.btnDetail.Name = "btnDetail";
            this.btnDetail.Size = new System.Drawing.Size(12, 23);
            this.btnDetail.MouseClick += new System.EventHandler<DSkin.DirectUI.DuiMouseEventArgs>(this.btnDetail_MouseClick);
            // 
            // lblTitle
            // 
            this.lblTitle.Font = new System.Drawing.Font("微软雅黑", 11F);
            this.lblTitle.ForeColor = System.Drawing.Color.White;
            this.lblTitle.Location = new System.Drawing.Point(13, 12);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(124, 21);
            this.lblTitle.Text = "Dui设计模式";
            // 
            // LeftListTemplate
            // 
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(114)))), ((int)(((byte)(158)))));
            this.Borders.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.Borders.LeftWidth = 0;
            this.Controls.AddRange(new DSkin.DirectUI.DuiBaseControl[] {
            this.btnDetail,
            this.lblTitle});
            this.Size = new System.Drawing.Size(188, 44);
            this.IsSelectedChanged += new System.EventHandler(this.LeftListTemplate_IsSelectedChanged);
            this.MouseEnter += new System.EventHandler<System.Windows.Forms.MouseEventArgs>(this.LeftListTemplate_MouseEnter);
            this.MouseLeave += new System.EventHandler(this.LeftListTemplate_MouseLeave);

        }

        #endregion

        private DSkin.DirectUI.DuiIcon btnDetail;
        private DSkin.DirectUI.DuiLabel lblTitle;
    }
}
