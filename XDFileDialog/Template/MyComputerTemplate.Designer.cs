﻿namespace XDFileDialog.Template
{
    partial class MyComputerTemplate
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.picIcon = new DSkin.DirectUI.DuiPictureBox();
            this.lblTitle = new DSkin.DirectUI.DuiLabel();
            this.pgbCapacity = new DSkin.DirectUI.DuiProgressBar();
            this.lblContent = new DSkin.DirectUI.DuiLabel();
            // 
            // picIcon
            // 
            this.picIcon.AutoSize = false;
            this.picIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picIcon.Borders.AllColor = System.Drawing.Color.Transparent;
            this.picIcon.Borders.BottomColor = System.Drawing.Color.Transparent;
            this.picIcon.Borders.LeftColor = System.Drawing.Color.Transparent;
            this.picIcon.Borders.RightColor = System.Drawing.Color.Transparent;
            this.picIcon.Borders.TopColor = System.Drawing.Color.Transparent;
            this.picIcon.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.picIcon.Images = null;
            this.picIcon.Location = new System.Drawing.Point(11, 4);
            this.picIcon.Name = "picIcon";
            this.picIcon.Size = new System.Drawing.Size(50, 50);
            this.picIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.DesignModeCanResize = false;
            this.lblTitle.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblTitle.Location = new System.Drawing.Point(70, 6);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(55, 18);
            this.lblTitle.Text = "Win7(C:)";
            // 
            // pgbCapacity
            // 
            this.pgbCapacity.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.pgbCapacity.Borders.AllColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.pgbCapacity.Borders.BottomColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.pgbCapacity.Borders.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.pgbCapacity.Borders.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.pgbCapacity.Borders.TopColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.pgbCapacity.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.pgbCapacity.ForeColors = new System.Drawing.Color[] {
        System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(160)))), ((int)(((byte)(218)))))};
            this.pgbCapacity.ForeColorsAngle = 90F;
            this.pgbCapacity.Location = new System.Drawing.Point(72, 23);
            this.pgbCapacity.Name = "pgbCapacity";
            this.pgbCapacity.Size = new System.Drawing.Size(167, 15);
            this.pgbCapacity.Text = "20%";
            this.pgbCapacity.Value = 20;
            // 
            // lblContent
            // 
            this.lblContent.AutoSize = true;
            this.lblContent.DesignModeCanResize = false;
            this.lblContent.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblContent.ForeColor = System.Drawing.Color.Gray;
            this.lblContent.Location = new System.Drawing.Point(71, 40);
            this.lblContent.Name = "lblContent";
            this.lblContent.Size = new System.Drawing.Size(142, 18);
            this.lblContent.Text = "25.33GB可用，共100GB";
            // 
            // MyComputerTemplate
            // 
            this.BackColor = System.Drawing.Color.White;
            this.Controls.AddRange(new DSkin.DirectUI.DuiBaseControl[] {
            this.picIcon,
            this.lblTitle,
            this.pgbCapacity,
            this.lblContent});
            this.Margin = new System.Windows.Forms.Padding(20, 10, 0, 0);
            this.Size = new System.Drawing.Size(250, 60);
            this.IsSelectedChanged += new System.EventHandler(this.MyComputerTemplate_IsSelectedChanged);
            this.MouseEnter += new System.EventHandler<System.Windows.Forms.MouseEventArgs>(this.MyComputerTemplate_MouseEnter);
            this.MouseLeave += new System.EventHandler(this.MyComputerTemplate_MouseLeave);
            this.MouseDoubleClick += new System.EventHandler<DSkin.DirectUI.DuiMouseEventArgs>(this.MyComputerTemplate_MouseDoubleClick);

        }

        #endregion

        private DSkin.DirectUI.DuiPictureBox picIcon;
        private DSkin.DirectUI.DuiLabel lblTitle;
        private DSkin.DirectUI.DuiProgressBar pgbCapacity;
        private DSkin.DirectUI.DuiLabel lblContent;
    }
}
