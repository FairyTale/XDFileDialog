﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XDFileDialog.Model
{
   public class LeftModel
    {
       public int Id { set; get; }
       public string Name { set; get; }
       public string Path { set; get; }
       /// <summary>
       /// 是否自定义
       /// </summary>
       public bool Type { set; get; }
    }
}
