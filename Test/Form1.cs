﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Test
{
    public partial class Form1 : DSkin.Forms.DSkinForm
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void xdOpenFileDialog1_FileOk(object sender, string path)
        {
            MessageBox.Show(path);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            xdOpenFileDialog1.Filter = "可执行文件(*.exe)|*.exe|所有文件(*.*)|*.*";
            xdOpenFileDialog1.ShowDialog();
        }
    }
}
