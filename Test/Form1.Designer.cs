﻿namespace Test
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.xdOpenFileDialog1 = new XDFileDialog.XDOpenFileDialog(this.components);
            this.xdOpenFileDialog2 = new XDFileDialog.XDOpenFileDialog(this.components);
            this.SuspendLayout();
            // 
            // xdOpenFileDialog1
            // 
            this.xdOpenFileDialog1.Filter = "所有文件(*.*)|*.*";
            this.xdOpenFileDialog1.FilterIndex = 0;
            this.xdOpenFileDialog1.Path = null;
            this.xdOpenFileDialog1.PathFileName = null;
            this.xdOpenFileDialog1.SafeFileName = null;
            this.xdOpenFileDialog1.FileOk += new XDFileDialog.XDOpenFileDialog.FileOkEventArgs(this.xdOpenFileDialog1_FileOk);
            // 
            // xdOpenFileDialog2
            // 
            this.xdOpenFileDialog2.Filter = "所有文件(*.*)|*.*";
            this.xdOpenFileDialog2.FilterIndex = 0;
            this.xdOpenFileDialog2.Path = null;
            this.xdOpenFileDialog2.PathFileName = null;
            this.xdOpenFileDialog2.SafeFileName = null;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(520, 262);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private XDFileDialog.XDOpenFileDialog xdOpenFileDialog1;
        private XDFileDialog.XDOpenFileDialog xdOpenFileDialog2;
    }
}

